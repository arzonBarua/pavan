-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 01, 2017 at 09:36 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pavan`
--

-- --------------------------------------------------------

--
-- Table structure for table `acl_management`
--

CREATE TABLE `acl_management` (
  `id` int(11) NOT NULL,
  `login_id` int(11) NOT NULL,
  `access` varchar(250) NOT NULL,
  `created_date` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acl_management`
--

INSERT INTO `acl_management` (`id`, `login_id`, `access`, `created_date`, `modified`) VALUES
(7, 29, '{"1":["2","3"],"6":["7","8"]}', '2016-11-25', '0000-00-00'),
(8, 1, '{"1":["2","3","4","5"]}', '2016-11-25', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE `admin_login` (
  `id` int(11) NOT NULL,
  `full_name` varchar(120) NOT NULL,
  `email` varchar(120) NOT NULL,
  `department` varchar(100) NOT NULL,
  `designation` varchar(120) NOT NULL,
  `picture` varchar(250) NOT NULL,
  `user_type` int(11) NOT NULL,
  `ip_restriction` tinyint(4) NOT NULL,
  `ip_address` varchar(250) NOT NULL,
  `password` varchar(200) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`id`, `full_name`, `email`, `department`, `designation`, `picture`, `user_type`, `ip_restriction`, `ip_address`, `password`, `created_at`, `updated_at`) VALUES
(2, 'Martin', 'arzon.barua@bol-online.com', 'Tutor', 'Super Admin', 'f79613575946de3d88fff5c356a9f4bc.png', 1, 0, '', 'e10adc3949ba59abbe56e057f20f883e', '2016-11-10', '2016-12-22');

-- --------------------------------------------------------

--
-- Table structure for table `admin_type`
--

CREATE TABLE `admin_type` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_type`
--

INSERT INTO `admin_type` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', '2016-11-09', '0000-00-00'),
(2, 'Admin', '2016-11-14', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `cms`
--

CREATE TABLE `cms` (
  `id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `url` varchar(120) NOT NULL,
  `menupos` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `footer_position` int(11) NOT NULL,
  `customurl` varchar(120) NOT NULL,
  `content` text NOT NULL,
  `styletype` text NOT NULL,
  `scripttype` text NOT NULL,
  `metakeyword` text NOT NULL,
  `metadescp` text NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms`
--

INSERT INTO `cms` (`id`, `title`, `url`, `menupos`, `position`, `footer_position`, `customurl`, `content`, `styletype`, `scripttype`, `metakeyword`, `metadescp`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Test Seite', 'TestSeite', 3, 1, 3, '', '<p></p>\r\n<pre class="prettyprint lang-auto linenums:0 prettyprinted"><span class="tag">&lt;ul</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"faq"</span><span class="tag">&gt;</span><span class="pln">\r\n&nbsp; </span><span class="tag"></span></pre>\r\n<ul>\r\n<li></li>\r\n</ul>\r\n<pre class="prettyprint lang-auto linenums:0 prettyprinted"><span class="tag"></span><span class="pln">\r\n&nbsp; &nbsp; </span><span class="tag">&lt;a</span><span class="pln"> </span><span class="atn">href</span><span class="pun">=</span><span class="atv">"#"</span><span class="pln"> </span><span class="atn">name</span><span class="pun">=</span><span class="atv">"faq-1"</span><span class="tag">&gt;</span><span class="pln">First question is?</span><span class="tag">&lt;br</span><span class="pln"> </span><span class="tag">/&gt;</span><span class="pln">\r\n&nbsp; &nbsp; </span><span class="tag">&lt;div</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"answer"</span><span class="pln"> </span><span class="atn">id</span><span class="pun">=</span><span class="atv">"faq-1"</span><span class="tag">&gt;</span><span class="pln">\r\n&nbsp; &nbsp; &nbsp; </span><span class="tag"></span></pre>\r\n<p></p>\r\n<pre class="prettyprint lang-auto linenums:0 prettyprinted"><span class="tag"></span><span class="pln">Answer to number one.</span><span class="tag"></span><span class="pln">\r\n&nbsp; &nbsp; </span><span class="tag"></span><span class="pln">\r\n&nbsp; </span><span class="tag"></span><span class="pln">\r\n&nbsp; </span><span class="tag"></span></pre>\r\n<ul>\r\n<li></li>\r\n</ul>\r\n<pre class="prettyprint lang-auto linenums:0 prettyprinted"><span class="tag"></span><span class="pln">\r\n&nbsp; &nbsp; </span><span class="tag">&lt;a</span><span class="pln"> </span><span class="atn">href</span><span class="pun">=</span><span class="atv">"#"</span><span class="pln"> </span><span class="atn">name</span><span class="pun">=</span><span class="atv">"faq-2"</span><span class="tag">&gt;</span><span class="pln">Second question is?</span><span class="tag">&lt;br</span><span class="pln"> </span><span class="tag">/&gt;</span><span class="pln">\r\n&nbsp; &nbsp; </span><span class="tag">&lt;div</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"answer"</span><span class="pln"> </span><span class="atn">id</span><span class="pun">=</span><span class="atv">"faq-2"</span><span class="tag">&gt;</span><span class="pln">\r\n&nbsp; &nbsp; &nbsp; </span><span class="tag"></span></pre>\r\n<p></p>\r\n<pre class="prettyprint lang-auto linenums:0 prettyprinted"><span class="tag"></span><span class="pln">Answer to number two.</span><span class="tag"></span><span class="pln">\r\n&nbsp; &nbsp; </span><span class="tag"></span><span class="pln">\r\n&nbsp; </span><span class="tag"></span><span class="pln">\r\n</span><span class="tag"></span></pre>', '#faqs dt, #faqs dd { padding: 0 0 0 50px }\r\n#faqs dt { font-size:1.5em; color: #9d9d9d; cursor: pointer; height: 37px; line-height: 37px; margin: 0 0 15px 25px}\r\n#faqs dd { font-size: 1em; margin: 0 0 20px 25px}\r\n#faqs dt { background: url(http://www.designonslaught.com/files/2012/06/expand-icon.png) no-repeat left}\r\n#faqs .expanded { background: url(http://www.designonslaught.com/files/2012/06/expanded-icon.png) no-repeat left}', '$(document).ready(function() {\r\n	$(''.answer'').hide();									\r\n	$("a[name^=''faq-'']").each(function() {\r\n		$(this).click(function() {\r\n			if($("#"+this.name).is('':hidden'')) {\r\n				$("#"+this.name).fadeIn(''slow'');\r\n			} else {\r\n				$("#"+this.name).fadeOut(''slow'');\r\n			}\r\n		});\r\n	});\r\n	$(''.answer'').click(function() {\r\n		$(this).fadeOut();\r\n	});\r\n});', 'test', 'test', 0, '0000-00-00', '2017-01-06'),
(6, 'So funktioniert''s', 'so-funktionierts', 1, 1, 0, '', '<h1><img src="/assets/images/sofunktionierts8x13.png" alt="SoFunktionierts" title="SoFunktionierts" /></h1>', '', '', '', '', 1, '2016-12-11', '2017-01-21'),
(7, 'Danke-1', 'danke-1', 2, 0, 1, '', '<p>Lieber Kunde,</p>\r\n<p></p>\r\n<p>wir haben Ihren Wunsch nach einem unverbindlichen Angebot, bzw. Ihren Auftrag Ihre Vertr&auml;ge zu wechseln erhalten!</p>\r\n<p>Wenn Sie uns Ihre Tarifdaten nicht im Formular &uuml;bermittelt haben: <span style="color: #ff0000;"><strong>Bitte vergessen Sie nicht, uns Ihre letzte Jahresrechnung Strom und/oder Gas zukommen zu lassen.</strong></span></p>\r\n<p>&nbsp;</p>\r\n<p></p>\r\n<p><strong>Wie geht es jetzt weiter?</strong></p>\r\n<p>Wenn Sie ein Angebot angefordert haben, werden wir Ihnen in K&uuml;rze einen geeigneten Lieferanten vorschlagen. Wenn Sie &uuml;berzeugt sind, k&ouml;nnen Sie uns im Anschluss mit dem Wechsel beauftragen.</p>\r\n<p>Wenn Sie uns bereits zum Wechseln Ihres Strom- und/oder Gas-Liefervertrags beauftragt haben, werden wir - sobald wir alle notwendigen Daten von Ihnen erhalten haben - Ihren Lieferanten wechseln und Sie nach erfolgreichem Wechsel &uuml;ber Ihren neuen Lieferanten sowie Ihre Ersparnis informieren. Der Wechsel kann in wenigen F&auml;llen einige Wochen in Anspruch nehmen, werden Sie also bitte nicht nerv&ouml;s wenn Sie nicht schon in den n&auml;chsten Tagen von uns h&ouml;ren!</p>\r\n<p></p>\r\n<p><br /> Vielen Dank f&uuml;r Ihr Vertrauen!</p>', '', '', '', '', 0, '2016-12-14', '2017-01-21'),
(8, 'ÜBER UNS', 'ueberuns', 3, 5, 1, '', '<p style="text-align: center;"><span style="font-size: medium;"><strong>Wer steckt hinter WECHSELSTROM?</strong></span></p>\r\n<p style="text-align: center;"></p>\r\n<p style="text-align: center;"><span style="font-size: medium;">Gegr&uuml;ndet wurde die WECHSELSTROM Tronnier &amp; Vo&szlig; GbR von zwei Mathematik-Absolventen der RWTH Aachen: Markus Tronnier und Martin Vo&szlig;.</span></p>\r\n<p style="text-align: center;"><img src="/assets/images/BildTronnierVoss.png" alt="sdfsdf" />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p>\r\n<p style="text-align: center;"></p>\r\n<p style="text-align: center;"><span style="font-size: medium;">Wir sind beruflich bereits seit mehreren Jahren in der Energiewirtschaft t&auml;tig.</span></p>\r\n<p style="text-align: center;"><span style="font-size: medium;">Privat nutzen wir seit nunmehr 10 Jahren die Vorteile der liberalisierten Energiewirtschaft und wechseln Strom- und Gastarife.</span><br /><span style="font-size: medium;">Da die Nachfrage nach unserer Dienstleistung kontinuierlich gestiegen ist, haben wir uns k&uuml;rzlich dazu entschlossen, die WECHSELSTROM Tronnier &amp; Vo&szlig; GbR zu gr&uuml;nden.</span></p>\r\n<p style="text-align: center;"></p>', '', '', '', '', 1, '2017-01-06', '2017-01-11'),
(9, 'Impressum', 'impressum', 2, 0, 5, '', '<p><strong style="font-size: large;">Impressum</strong></p>\r\n<p><strong style="font-size: large;">sowie Anbieterkennung<br /></strong><span style="font-size: large;"><strong>im Sinne von &sect;&sect; 5 TMG, 55 RStV</strong></span></p>\r\n<p></p>\r\n<p><span style="font-size: medium;">Tronnier &amp; Vo&szlig; GbR</span><br /><span style="font-size: medium;"> Reimanstra&szlig;e, 17</span><br /><span style="font-size: medium;"> 52070 Aachen</span><br /><span style="font-size: medium;"> E-Mail: <a href="mailto:info@wechselstrom-ac.de">info@wechselstrom-ac.de<br /></a></span><span style="font-size: medium;">Web: <a href="http://www.wechselstrom-ac.de">www.wechselstrom-ac.de<br /></a></span><span style="font-size: medium;">Telefon: 0159 / 05 43 15 71<br /></span><span style="font-size: medium;">Fax: 0241 / 92 78 88 37</span></p>\r\n<p><span style="font-size: medium;"><strong>Vertretungsberechtigte Gesellschafter:</strong></span></p>\r\n<p><span style="font-size: medium;"><strong><strong>Martin Vo&szlig;<br /></strong></strong></span><span style="font-size: medium;"><strong>MarkusTronnier</strong></span></p>\r\n<p></p>\r\n<p><em><span style="font-size: small;">Informationen zur Online-Streitbeilegung: Die EU-Kommission hat im ersten Quartal 2016 eine Internetplattform zur Online-Beilegung von Streitigkeiten (sog. &bdquo;OS-Plattform&ldquo;) bereitgestellt. Die OS-Plattform soll als Anlaufstelle zur au&szlig;ergerichtlichen Beilegung von Streitigkeiten betreffend vertragliche Verpflichtungen, die aus Online-Kaufvertr&auml;gen erwachsen, dienen. Die OS-Plattform wird unter folgendem Link erreichbar sein:&nbsp;<a href="http://ec.europa.eu/consumers/odr">http://ec.europa.eu/consumers/odr</a></span><br /></em></p>', '', '', '', '', 1, '2017-01-07', '2017-01-08'),
(10, 'Preis & Beispiel', 'preis-beispiel', 1, 1, 0, '', '<p style="text-align: center;"><span style="font-size: medium;">WECHSELSTROM arbeitet auf Provisionsbasis. <strong>Die Provision liegt bei 30% der Ersparnis</strong>, die WECHSELSTROM f&uuml;r Sie erreicht. Daraus folgt:</span></p>\r\n<p style="text-align: center;"><span style="font-size: medium;"><strong>Wenn WECHSELSTROM Ihnen kein Geld spart, wird auch keine Provision f&auml;llig.<br /><br /></strong></span></p>\r\n<p style="text-align: center;"><span style="font-size: medium;">Wir m&ouml;chten Ihnen anhand eines Beispiels aufzeigen, wie sich die H&ouml;he der Provision bestimmt:</span></p>\r\n<p style="text-align: center;"><span style="font-size: medium;"> Frau Vo&szlig; wird derzeit durch den Lieferanten &bdquo;A&ldquo; mit Strom versorgt. Sie verbraucht gesch&auml;tzt&nbsp;<strong>4.000 kWh Strom</strong> pro Jahr. Zum 01.01. des Folgejahres wird Lieferant &bdquo;A&ldquo; seine Preise erh&ouml;hen.<br /></span><span style="font-size: medium;">Frau Vo&szlig; zahlt dann als Grundpreis <strong>10 &euro; / Monat</strong> sowie als Arbeitspreis<strong> 0,28 &euro; / kWh</strong>.<br /></span><span style="font-size: medium;">In den folgenden 12 Monaten w&uuml;rde Frau Vo&szlig; bei Lieferant A also gesch&auml;tzte Kosten von</span></p>\r\n<p style="text-align: center;"><br /><span style="font-size: medium;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Grundpreis: 10 &euro;/Monat * 12 Monate = 120 &euro;</span><br /><span style="font-size: medium;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Arbeitspreis: 0,28 &euro;/kWh * 4000 kWh = 1.120 &euro;</span><br /><span style="font-size: medium;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; = <span style="color: #ff0000;"><strong>1.240 &euro;<br /><br /></strong></span></span></p>\r\n<p style="text-align: center;"><span style="font-size: medium;"><span style="color: #ff0000;"><strong></strong></span></span><span style="font-size: medium;">haben. Alternativ hat Sie die Wahl ihren Stromvertag zum 01.01. zu wechseln. Sie beauftragt WECHSELSTROM damit. WECHSELSTROM&nbsp;findet f&uuml;r sie den g&uuml;nstigen Lieferanten &bdquo;B&ldquo;.<br /> Dieser fordert ebenfalls <strong>10 &euro; / Monat</strong> als Grundpreis, aber nur <strong>0,23 &euro; / kWh</strong> als Arbeitspreis. <br />Dazu bietet er einen <strong>Neukundenbonus von 100 &euro;</strong>, der nach 12 Monaten Belieferung ausgezahlt wird.<br /> Die prognostizierten Stromkosten bei Lieferant &bdquo;B&ldquo; liegen damit bei:</span></p>\r\n<p style="text-align: center;"><br /><span style="font-size: medium;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Grundpreis: 10 &euro;/Monat * 12 Monate = 120 &euro;</span><br /><span style="font-size: medium;"> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+ Arbeitspreis: 0,23 &euro;/kWh * 4000 = 920 &euro;</span><br /><span style="font-size: medium;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Neukundenbonus: 100 &euro;</span><br /><span style="font-size: medium;"> &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<span style="color: #99cc00;"><strong> 940 &euro;<br /><br /></strong></span></span></p>\r\n<p style="text-align: center;"><span style="font-size: medium;"><span style="color: #99cc00;"><strong></strong></span></span><span style="font-size: medium;">Durch einen Wechsel zu Lieferant &bdquo;B&ldquo;<span> <strong>spart Frau Vo&szlig; im Folgejahr 300 &euro;!</strong></span></span></p>\r\n<p style="text-align: center;"><span style="font-size: medium;">Die Provision f&uuml;r Wechselstrom liegt bei 30 % der Ersparnis von 300 &euro;, also 0,3*300 &euro; = <strong>90 &euro;</strong>.</span></p>\r\n<p style="text-align: center;"><span style="font-size: medium;"><span style="color: #99cc00;"><strong>"Klasse", denkt sich Frau Vo&szlig;, denn sie hat ohne Arbeit 210 &euro; mehr auf dem Konto</strong>.</span> <br />Und weil sie bereits Kunde bei WECHSELSTROM ist, muss Sie sich fortan um nichts mehr k&uuml;mmern. Nur wenn Sie mal umzieht, sollte sie Bescheid geben!</span></p>', '', '', '', '', 1, '2017-01-07', '2017-01-22'),
(12, 'Noch Fragen?', 'noch-fragen', 1, 5, 0, '', '<div class="container">\r\n<div class="page-header">\r\n<h1>FAQ <small>Allgemein</small></h1>\r\n</div>\r\n<div class="row">\r\n<div id="faq" class="col-md-9">\r\n<div class="panel-group" id="accordion">\r\n<div class="panel panel-default">\r\n<div class="panel-heading">\r\n<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-1">Welches Risiko besteht, wenn ich meine Strom- und Gastarife von WECHSELSTROM &uuml;berpr&uuml;fen lasse?</a></h4>\r\n</div>\r\n<div id="collapse-1" class="panel-collapse collapse">\r\n<div class="panel-body">F&uuml;r Sie besteht keinerlei Risiko! WECHSELSTROM pr&uuml;ft regelm&auml;&szlig;ig die Liquidit&auml;t neuer Lieferanten und w&auml;hlt keine Tarife f&uuml;r Sie aus, bei denen Sie in Vorkasse treten. Au&szlig;erdem wird ein Wechsel nur veranlasst, wenn der neue Vertrag f&uuml;r Sie g&uuml;nstiger ist. Auch finanziell besteht f&uuml;r Sie keinerlei Risiko, da WECHSELSTROM keine Geb&uuml;hren erhebt, sondern lediglich eine Provision in H&ouml;he von 30% der erzielten Kostenreduzierung in Rechnung stellt. Sie werden somit in jedem Fall weniger Geld zahlen, als ohne den Wechsel durch WECHSELSTROM.</div>\r\n<div class="panel-footer"></div>\r\n</div>\r\n</div>\r\n<div class="panel panel-default">\r\n<div class="panel-heading">\r\n<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-2">In wie fern unterscheidet sich WECHSELSTROM von anderen, bekannten Vergleichsportalen?</a></h4>\r\n</div>\r\n<div id="collapse-2" class="panel-collapse collapse">\r\n<div class="panel-body">Zun&auml;chst einmal m&uuml;ssen Sie keinen alternativen Anbieter suchen, dies wird f&uuml;r Sie von WECHSELSTROM &uuml;bernommen. Und anschlie&szlig;end, darin besteht der gr&ouml;&szlig;te Unterschied, veranlasst WECHSELSTROM f&uuml;r Sie den Wechsel. Und das nicht nur einmalig, sondern jedes Jahr, damit Sie immer von einem f&uuml;r sie g&uuml;nstigen Lieferanten beliefert werden.</div>\r\n<div class="panel-footer"></div>\r\n</div>\r\n</div>\r\n<div class="panel panel-default">\r\n<div class="panel-heading">\r\n<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-3">Ist WECHSELSTROM unabh&auml;ngig, oder arbeitet WECHSELSTROM mit Strom- und Gaslieferanten zusammen?</a></h4>\r\n</div>\r\n<div id="collapse-3" class="panel-collapse collapse">\r\n<div class="panel-body">WECHSELSTROM ist absolut unabh&auml;ngig und immer auf der Suche nach dem besten Tarif f&uuml;r Sie. Da die Provision von WECHSELSTROM von der H&ouml;he der Kostenreduzierungen abh&auml;ngt, ist das Bestreben von WECHSELSTROM immer den besten Tarif f&uuml;r Sie zu finden.</div>\r\n<div class="panel-footer"></div>\r\n</div>\r\n</div>\r\n<div class="panel panel-default">\r\n<div class="panel-heading">\r\n<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-4">Warum sollte ich Kunde bei WECHSELSTROM werden, anstatt meine Vertr&auml;ge selber zu &uuml;berpr&uuml;fen?</a></h4>\r\n</div>\r\n<div id="collapse-4" class="panel-collapse collapse">\r\n<div class="panel-body">Da WECHSELSTROM jedes Jahr f&uuml;r Sie den Anbieter wechselt und keine Frist verpasst. Sollten Sie Ihre Vertr&auml;ge selber wechseln besteht f&uuml;r Sie das Risiko, den K&uuml;ndigungstermin f&uuml;r den alten Vertrag zu verpassen und anschlie&szlig;end h&ouml;here Geb&uuml;hren zu zahlen als im Vorjahr, da die Energiekosten von Jahr zu Jahr variieren k&ouml;nnen. Au&szlig;erdem m&uuml;ssen Sie sich nicht Jahr f&uuml;r Jahr auf die Suche nach einem neuen Lieferanten machen, das &uuml;bernimmt WECHSELSTROM f&uuml;r Sie.</div>\r\n<div class="panel-body"></div>\r\n<div class="panel-footer"></div>\r\n</div>\r\n</div>\r\n<div class="panel panel-default">\r\n<div class="panel-heading">\r\n<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-5">Kann es passieren, dass ich durch die Beauftragung von WECHSELSTROM einmal ohne Strom oder Gas dastehe?</a></h4>\r\n</div>\r\n<div id="collapse-5" class="panel-collapse collapse">\r\n<div class="panel-body">Nein, das ist nicht m&ouml;glich. Sollte es unerwartet zu Problemen mit dem neuen Lieferanten kommen, springt der Grundversorger ein und beliefert Sie mit Strom und Gas.</div>\r\n<div class="panel-footer"></div>\r\n</div>\r\n</div>\r\n<div class="page-header">\r\n<h2 style="font-size: 36px;">FAQ&nbsp;<small>Angebot anfordern</small></h2>\r\n</div>\r\n<div class="panel panel-default">\r\n<div class="panel-heading">\r\n<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-6">Welche Kosten entstehen bei Anforderung eines Angebots von WECHSELSTROM?</a></h4>\r\n</div>\r\n<div id="collapse-6" class="panel-collapse collapse">\r\n<div class="panel-body">Die Anforderung eines Angebots und somit &Uuml;berpr&uuml;fung Ihrer Tarife durch WECHSELSTROM ist kostenlos. Sofern WECHSELSTROM f&uuml;r Sie einen g&uuml;nstigeren Tarif bei einem neuen Lieferanten als Ihren aktuellen Tarif findet, Sie WECHSELSTROM beauftragen und WECHSELSTROM einen Wechsel Ihres Tarifs durchf&uuml;hrt, erh&auml;lt WECHSELSTROM eine Provision in H&ouml;he von 30 Prozent der Kostenreduzierungen, die WECHSELSTROM f&uuml;r Sie durch den Tarifwechsel erzielt.</div>\r\n<div class="panel-footer"></div>\r\n</div>\r\n</div>\r\n<div class="panel panel-default">\r\n<div class="panel-heading">\r\n<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-62">In wie fern unterscheidet sich WECHSELSTROM von anderen, bekannten Vergleichsportalen?</a></h4>\r\n</div>\r\n<div id="collapse-62" class="panel-collapse collapse">\r\n<div class="panel-body">Zun&auml;chst einmal m&uuml;ssen Sie keinen alternativen Anbieter suchen, dies wird f&uuml;r Sie von WECHSELSTROM &uuml;bernommen. Und anschlie&szlig;end, darin besteht der gr&ouml;&szlig;te Unterschied, veranlasst WECHSELSTROM f&uuml;r Sie den Wechsel. Und das nicht nur einmalig, sondern jedes Jahr, damit Sie immer von einem f&uuml;r sie g&uuml;nstigen Lieferanten beliefert werden.</div>\r\n<div class="panel-footer"></div>\r\n</div>\r\n</div>\r\n<div class="panel panel-default">\r\n<div class="panel-heading">\r\n<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-7">Wo finde ich meine Z&auml;hlernummer?</a></h4>\r\n</div>\r\n<div id="collapse-7" class="panel-collapse collapse">\r\n<div class="panel-body">Die Z&auml;hlernummer l&auml;sst sich sowohl von der letzten Strom- bzw. Gasrechnung, als auch am Strom- oder Gasz&auml;hler ablesen.</div>\r\n<div class="panel-footer"></div>\r\n</div>\r\n</div>\r\n<div class="panel panel-default">\r\n<div class="panel-heading">\r\n<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-8">Wo finde ich meinen Strom- oder Gasz&auml;hler?</a></h4>\r\n</div>\r\n<div id="collapse-8" class="panel-collapse collapse">\r\n<div class="panel-body">In der Regel befinden sich die Z&auml;hler in den Kellerr&auml;umen. Sollten Sie ihn dort nicht finden k&ouml;nnen, wenden Sie sich am besten an die Hausverwaltung bzw. den Vermieter.</div>\r\n<div class="panel-footer"></div>\r\n</div>\r\n</div>\r\n<div class="panel panel-default">\r\n<div class="panel-heading">\r\n<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-9">Frage?</a></h4>\r\n</div>\r\n<div id="collapse-9" class="panel-collapse collapse">\r\n<div class="panel-body">Antwort!</div>\r\n<div class="panel-footer"></div>\r\n</div>\r\n</div>\r\n<div class="panel panel-default">\r\n<div class="panel-heading">\r\n<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-10">Frage?</a></h4>\r\n</div>\r\n<div id="collapse-10" class="panel-collapse collapse">\r\n<div class="panel-body">Antwort!</div>\r\n<div class="panel-footer"></div>\r\n</div>\r\n</div>\r\n<div class="panel panel-default">\r\n<div class="panel-heading">\r\n<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-11">Frage</a></h4>\r\n</div>\r\n<div id="collapse-11" class="panel-collapse collapse">\r\n<div class="panel-body">Antwort</div>\r\n<div class="panel-footer"></div>\r\n</div>\r\n</div>\r\n<div class="page-header">\r\n<h3 style="font-size: 36px;">FAQ&nbsp;<small>Kunde werden</small></h3>\r\n</div>\r\n<div class="panel panel-default">\r\n<div class="panel-heading">\r\n<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-12">Welche Kosten entstehen bei einer Beauftragung von WECHSELSTROM?</a></h4>\r\n</div>\r\n<div id="collapse-12" class="panel-collapse collapse">\r\n<div class="panel-body">Sofern WECHSELSTROM f&uuml;r Sie einen g&uuml;nstigeren Tarif bei einem neuen Lieferanten als Ihren aktuellen Tarif findet, Sie WECHSELSTROM beauftragen und WECHSELSTROM einen Wechsel Ihres Tarifs durchf&uuml;hrt, erh&auml;lt WECHSELSTROM eine Provision in H&ouml;he von 30 Prozent der Kostenreduzierungen, die WECHSELSTROM f&uuml;r Sie durch den Tarifwechsel erzielt.</div>\r\n<div class="panel-footer"></div>\r\n</div>\r\n</div>\r\n<div class="panel panel-default">\r\n<div class="panel-heading">\r\n<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-13">Was passiert,wenn ich umziehe?</a></h4>\r\n</div>\r\n<div id="collapse-13" class="panel-collapse collapse">\r\n<div class="panel-body">Im Falle eines Umzuges m&uuml;ssen Sie WECHSELSTROM informieren, damit wir die M&ouml;glichkeiten an Ihrem neuen Wohnort pr&uuml;fen k&ouml;nnen und ggf. die laufenden Vertr&auml;ge wechseln, in jedem Fall aber die derzeitigen Strom- bzw. Gaslieferanten informieren k&ouml;nnen.</div>\r\n<div class="panel-footer"></div>\r\n</div>\r\n</div>\r\n<div class="panel panel-default">\r\n<div class="panel-heading">\r\n<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-14">Wie lange werde ich bei einem Tarifwechsel an den neuen Lieferanten gebunden sein?</a></h4>\r\n</div>\r\n<div id="collapse-14" class="panel-collapse collapse">\r\n<div class="panel-body">In der Regel betragen die Mindestlaufzeiten der Strom- und Gasvertr&auml;ge 12 Monate. Wenn Sie aber bereits umziehen und nicht die M&ouml;glichkeit besteht, Ihren Tarif mit umzuziehen, haben Sie ein gesetzliches Sonderk&uuml;ndigungsrecht.</div>\r\n<div class="panel-footer"></div>\r\n</div>\r\n</div>\r\n<div class="panel panel-default">\r\n<div class="panel-heading">\r\n<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-15">Frage?</a></h4>\r\n</div>\r\n<div id="collapse-15" class="panel-collapse collapse">\r\n<div class="panel-body">Antwort!</div>\r\n<div class="panel-footer"></div>\r\n</div>\r\n</div>\r\n<div class="panel panel-default">\r\n<div class="panel-heading">\r\n<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-16">Frage?</a></h4>\r\n</div>\r\n<div id="collapse-16" class="panel-collapse collapse">\r\n<div class="panel-body">Antwort!</div>\r\n<div class="panel-footer"></div>\r\n</div>\r\n</div>\r\n<div class="panel panel-default">\r\n<div class="panel-heading">\r\n<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-17">Frage?</a></h4>\r\n</div>\r\n<div id="collapse-17" class="panel-collapse collapse">\r\n<div class="panel-body">Antwort!</div>\r\n<div class="panel-footer"></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>', '', '', '', '', 1, '2017-01-10', '2017-01-24'),
(13, 'Datenschutzerklärung', 'Datenschutz', 2, 0, 4, '', '<p>&nbsp;</p>\r\n<h2>Datenschutzerkl&auml;rung</h2>\r\n<p>Die Nutzung unserer Seite ist ohne&nbsp; eine Angabe von personenbezogenen Daten m&ouml;glich. F&uuml;r die Nutzung einzelner&nbsp; Services unserer Seite k&ouml;nnen sich hierf&uuml;r abweichende Regelungen ergeben, die in&nbsp; diesem Falle nachstehend gesondert erl&auml;utert werden. Ihre personenbezogenen&nbsp; Daten (z.B. Name, Anschrift, E-Mail, Telefonnummer, u.&auml;.) werden von uns nur&nbsp; gem&auml;&szlig; den Bestimmungen des deutschen Datenschutzrechts verarbeitet. Daten sind dann&nbsp; personenbezogen, wenn sie eindeutig einer bestimmten nat&uuml;rlichen Person&nbsp; zugeordnet werden k&ouml;nnen. Die rechtlichen Grundlagen des Datenschutzes finden&nbsp; Sie im Bundesdatenschutzgesetz (BDSG) und dem Telemediengesetz (TMG). Nachstehende&nbsp; Regelungen informieren Sie insoweit &uuml;ber die Art, den Umfang und Zweck der&nbsp; Erhebung, die Nutzung und die Verarbeitung von personenbezogenen Daten durch&nbsp; den Anbieter</p>\r\n<p><strong>Tronnier &amp; Vo&szlig; GbR, Reimanstra&szlig;e 17, 52070 Aachen</strong></p>\r\n<p><strong>Tel: 0159 05431571</strong></p>\r\n<p><strong>Email: info@wechselstrom-ac.de</strong></p>\r\n<p>Wir weisen darauf hin, dass die&nbsp; internetbasierte Daten&uuml;bertragung Sicherheitsl&uuml;cken aufweist, ein l&uuml;ckenloser&nbsp; Schutz vor Zugriffen durch Dritte somit unm&ouml;glich ist.</p>\r\n<p>&nbsp;</p>\r\n<h4>Cookies</h4>\r\n<p>Wir&nbsp; verwenden auf unserer Seite sog. Cookies zum Wiedererkennen mehrfacher Nutzung unseres&nbsp; Angebots, durch denselben Nutzer/Internetanschlussinhaber. Cookies sind kleine&nbsp; Textdateien, die Ihr Internet-Browser auf Ihrem Rechner ablegt und speichert.&nbsp; Sie dienen dazu, unseren Internetauftritt und unsere Angebote zu optimieren. Es&nbsp; handelt sich dabei zumeist um sog. "Session-Cookies", die nach dem&nbsp; Ende Ihres Besuches wieder gel&ouml;scht werden.</p>\r\n<p>&nbsp;&nbsp;&nbsp; Teilweise&nbsp; geben diese Cookies jedoch Informationen ab, um Sie automatisch wieder zu&nbsp; erkennen. Diese Wiedererkennung erfolgt aufgrund der in den Cookies&nbsp; gespeicherten IP-Adresse. Die so erlangten Informationen dienen dazu, unsere&nbsp; Angebote zu optimieren und Ihnen einen leichteren Zugang auf unsere Seite zu&nbsp; erm&ouml;glichen.</p>\r\n<p>&nbsp;&nbsp;&nbsp; Sie k&ouml;nnen&nbsp; die Installation der Cookies durch eine entsprechende Einstellung Ihres&nbsp; Browsers verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall&nbsp; gegebenenfalls nicht s&auml;mtliche Funktionen unserer Website vollumf&auml;nglich nutzen&nbsp; k&ouml;nnen.</p>\r\n<p>&nbsp;</p>\r\n<h4>Kontaktm&ouml;glichkeit</h4>\r\n<p>Wir&nbsp; bieten Ihnen auf unserer Seite die M&ouml;glichkeit, mit uns per E-Mail und/oder&nbsp; &uuml;ber ein Kontaktformular in Verbindung zu treten. In diesem Fall werden die vom&nbsp; Nutzer gemachten Angaben zum Zwecke der Bearbeitung seiner Kontaktaufnahme&nbsp; gespeichert. Eine Weitergabe an Dritte erfolgt nicht. Ein Abgleich der so&nbsp; erhobenen Daten mit Daten, die m&ouml;glicherweise durch andere Komponenten unserer&nbsp; Seite erhoben werden, erfolgt ebenfalls nicht.</p>\r\n<p>&nbsp;</p>\r\n<h3>Auskunft/Widerruf/L&ouml;schung</h3>\r\n<p>Sie k&ouml;nnen&nbsp; sich aufgrund des Bundesdatenschutzgesetzes bei Fragen zur Erhebung, Verarbeitung&nbsp; oder Nutzung Ihrer personenbezogenen Daten und deren Berichtigung, Sperrung,&nbsp; L&ouml;schung oder einem Widerruf einer erteilten Einwilligung unentgeltlich an uns&nbsp; wenden. Wir weisen darauf hin, dass Ihnen ein Recht auf Berichtigung falscher&nbsp; Daten oder L&ouml;schung personenbezogener Daten zusteht, sollte diesem Anspruch&nbsp; keine gesetzliche Aufbewahrungspflicht entgegenstehen.</p>\r\n<p></p>\r\n<p><a target="_blank" href="https://www.ratgeberrecht.eu/leistungen/muster-datenschutzerklaerung.html">Muster-Datenschutzerkl&auml;rung</a> der <a target="_blank" href="https://www.ratgeberrecht.eu/">Anwaltskanzlei Wei&szlig; &amp; Partner</a></p>', '', '', '', '', 1, '2017-01-16', '2017-01-16');

-- --------------------------------------------------------

--
-- Table structure for table `email_body`
--

CREATE TABLE `email_body` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE latin1_german2_ci NOT NULL,
  `from` varchar(250) COLLATE latin1_german2_ci NOT NULL,
  `subject` text COLLATE latin1_german2_ci NOT NULL,
  `message` text COLLATE latin1_german2_ci NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

--
-- Dumping data for table `email_body`
--

INSERT INTO `email_body` (`id`, `name`, `from`, `subject`, `message`, `updated_at`) VALUES
(1, 'Wechselstrom AC', 'info@wechselstrom-ac.de', 'Eingangsbestätigung Angebotsanfrage / Wechselauftrag', '<!doctype html>\r\n<html>\r\n <body>\r\n <p>Lieber Kunde, <p>\r\n\r\n <p>wir haben Ihren Wunsch nach einem unverbindlichen Angebot, bzw. Ihren Auftrag Ihre Verträge zu wechseln erhalten!  <p>\r\n<p>Wie geht es jetzt weiter?<p>\r\n\r\nWenn Sie ein Angebot angefordert haben, werden wir Ihnen in Kürze einen geeigneten Lieferanten vorschlagen. Wenn Sie überzeugt sind, können Sie uns im Anschluss mit der Durchführung des Lieferantenwechsels beauftragen.<p>\r\n\r\nWenn Sie uns bereits zum Wechseln Ihres Strom- und/oder Gas-Liefervertrags beauftragt haben, werden wir - sobald wir alle notwendigen Daten von Ihnen erhalten haben - Ihren Lieferanten wechseln und Sie nach erfolgreichem Wechsel über Ihren neuen Lieferanten sowie Ihre Ersparnis informieren. Der Wechsel kann in wenigen Fällen einige Wochen in Anspruch nehmen, werden Sie also bitte nicht nervös wenn Sie nicht schon in den nächsten Tagen von uns hören!<p>\r\n<p> <br>\r\n\r\n\r\nVielen Dank für Ihr Vertrauen!<p>\r\n<p>\r\nMarkus Tronnier & Martin Voß<br>\r\nGeschäftsführer Tronnier & Voß GbR (Wechselstrom GbR)<br>\r\nE-Mail: info@wechselstrom-ac.de<br>\r\nTel / Whatsapp: 0159 05 44 18 59<br>\r\nFax-Nummer: 0241 92 78 88 37<br>\r\nReimanstraße 17, 52070 Aachen', '2016-12-28');

-- --------------------------------------------------------

--
-- Table structure for table `menu_admin`
--

CREATE TABLE `menu_admin` (
  `id` int(11) NOT NULL,
  `link` varchar(120) NOT NULL,
  `title` varchar(120) NOT NULL,
  `alice` varchar(120) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `acl_show` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_admin`
--

INSERT INTO `menu_admin` (`id`, `link`, `title`, `alice`, `parent_id`, `status`, `acl_show`) VALUES
(1, '', 'Benutzer', 'fw fa-user', 0, 1, 1),
(2, 'admin/user_admin/user_add', 'Hinzufügen', '', 1, 1, 1),
(3, 'admin/user_admin/user_view', 'Ansehen', '', 1, 1, 1),
(4, 'admin/user_admin/user_edit/', 'Editieren', '', 1, 0, 1),
(5, 'admin/user_admin/user_delete/', 'Löschen', '', 1, 0, 1),
(6, '', 'Berechtigungen', 'fw fa-sitemap', 0, 1, 1),
(7, 'admin/acl/acl_add', 'Hinzufügen', '', 6, 1, 1),
(8, 'admin/acl/acl_view', 'Ansehen', '', 6, 1, 1),
(9, 'admin/acl/acl_edit/', 'Editieren', '', 6, 0, 1),
(10, 'admin/acl/acl_delete/', 'Löschen', '', 6, 0, 1),
(11, '', 'CMS\r\n', 'fw fa-file-text', 0, 1, 1),
(12, 'admin/cms/cms_add', 'Hinzufügen', '', 11, 1, 1),
(13, 'admin/cms/cms_view', 'Ansehen', '', 11, 1, 1),
(14, 'admin/cms/cms_edit/', 'Editieren', '', 11, 0, 1),
(15, 'admin/cms/cms_delete/', 'Löschen', '', 11, 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acl_management`
--
ALTER TABLE `acl_management`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_login`
--
ALTER TABLE `admin_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_type`
--
ALTER TABLE `admin_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms`
--
ALTER TABLE `cms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_body`
--
ALTER TABLE `email_body`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_admin`
--
ALTER TABLE `menu_admin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acl_management`
--
ALTER TABLE `acl_management`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `admin_login`
--
ALTER TABLE `admin_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `admin_type`
--
ALTER TABLE `admin_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cms`
--
ALTER TABLE `cms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `email_body`
--
ALTER TABLE `email_body`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `menu_admin`
--
ALTER TABLE `menu_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
