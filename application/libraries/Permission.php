<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Permission {

    protected $CI;

    public function __construct()
    {
    // Assign the CodeIgniter super-object
        $this->CI =& get_instance();
        $this->CI->load->model('Menu_model');
        $this->CI->load->library('session');
    }

    public function has_permission($key,$value=null){


        if($this->CI->session->userdata('user_type')=='1'){
            return true;
        }


        $access = json_decode($this->CI->session->userdata('access'),true);


        if(empty($access)){
            return false;
        }

        if(empty($value)){
            if (array_key_exists($key,$access)){
                return true;
            }
            return false;
        }else{

            $this->check_by_database($value);

            $temp_id = $this->CI->session->userdata('temp_id');
            $temp_parent_id = $this->CI->session->userdata('temp_parent_id');

            if(isset($access[$temp_parent_id])) {
                if (in_array($temp_id, $access[$temp_parent_id])) {
                    return true;
                }
            }

            return false;
        }
    }

    public function check_by_database($id){
//        $sql = "SELECT id, parent_id FROM menu_admin where id=?";
//        $query = $this->CI->db->query($sql,array($id));
        $get_data = $this->CI->Menu_model->check_by_database($id);
        if($get_data[0]->parent_id != 0){
            $temp = array(
                'temp_id' => $get_data[0]->id,
                'temp_parent_id' => $get_data[0]->parent_id,
            );
            $this->CI->session->set_userdata($temp);
            $this->check_by_database($get_data[0]->parent_id);
        }
        //echo  $temp_parent_id.",".$temp_id;
    }


    public function check_moudel(){
        $full_url = base_url(uri_string());
        $base_url = $this->CI->config->item('base_url');
        $link_url = preg_replace('/[0-9]+/', '', str_replace($base_url,"",$full_url));
        $result = $this->CI->Menu_model->get_credential($link_url);

        if(isset($result[0]->parent_id) && isset($result[0]->id)) {
           return $this->has_permission($result[0]->parent_id, $result[0]->id);
        }
        return false;
    }

}