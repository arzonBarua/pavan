<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if(!($this->session->userdata('isLoged')) ){
            redirect('admin/index');
        }

        /*For ACL*/
        if($this->session->userdata('user_type')!='1'){
            $this->load->library('permission');
            if (!$this->permission->check_moudel()) {
                redirect('admin/home/deny_page');
            }
        }
        /*For ACL*/
        $this->load->library('form_validation');
        $this->load->model('Post_model');

    }

/*--------------------------------Menu--------------------------*/

    public function menu_add(){
        $data['menu_title'] = "Menu";
        if(empty($_POST)){
            $this->load->view('admin/menu/menu-add',$data);
        }else{
                $input['name'] = $this->input->post('name'); 
                $input['order_id'] = $this->input->post('order_id');              
                $input['created_at'] = date('Y-m-d');
                $this->Post_model->insert('menu',$input);
                redirect('admin/menu/menu_view');               
        }
    }

    public function menu_view(){
        $data['menu_title'] = "Menu";
        $data['result'] = $this->Post_model->getAll('menu','order_id','ASC');


        if($data['result'] === false)
            $this->session->set_flashdata('error_message', 'No data Found');

        $this->load->view('admin/menu/menu-view', $data);
    }

    public function menu_edit($id){
        $data['menu_title'] = "Menu";
        $data['result'] = $this->Post_model->getAll('menu','','',$id);

        if(empty($_POST)){
             if ($data['result'] === false)
                $this->session->set_flashdata('error_message', 'No data Found');
            $this->load->view('admin/menu/menu-edit',$data);
        }else{

            $input['name'] = $this->input->post('name');
            $input['order_id'] = $this->input->post('order_id');
            $input['modified_at'] = date('Y-m-d');

            $this->Post_model->update('menu',$input,$id);
            $this->session->set_flashdata('success_message', 'Data updated successfully');
            redirect('admin/menu/menu_view');

        }

    }

    public function menu_delete($id){
        $this->Post_model->delete('menu',$id);

        $this->session->set_flashdata('success_message', 'Data Deleted successfully');
        redirect('admin/menu/menu_view');
    }


/*----------------------End Menu---------------------*/

/*---------------Submenu-------------*/
    public function menu_sub_add(){
        $data['menu_title'] = "Submenu";
        if(empty($_POST)){
             $data['result'] = $this->Post_model->getAll('menu','order_id','ASC');

            $this->load->view('admin/menu/submenu-add',$data);
        }else{
            $input['menu_id'] = $this->input->post('menu_id'); 
            $input['name'] = $this->input->post('name');      
            $input['order_id'] = $this->input->post('order_id');          
            $input['created_at'] = date('Y-m-d');

            $this->Post_model->insert('submenu',$input);
            redirect('admin/menu/menu_sub_view');               
        }
    }

      public function menu_sub_view(){
        $data['menu_title'] = "Submenu";
        $data['result'] = $this->Post_model->getAll('submenu','order_id','ASC');
        
        $joinTable[0]['name'] = 'menu';
        $join_id[0]['name'] = 'menu_id';

        $data['result'] = $this->Post_model->join_table("submenu.id as id, menu.name as menu_name, submenu.name, submenu.order_id", "submenu", $join_id,$joinTable,"Join", "menu.order_id","ASC");


        if($data['result'] === false)
            $this->session->set_flashdata('error_message', 'No data Found');

        $this->load->view('admin/menu/submenu-view', $data);
    }


    public function menu_sub_edit($id){
        $data['menu_title'] = "Submenu";
        $data['result'] = $this->Post_model->getAll('submenu','','',$id);

        $data['menu_list'] = $this->Post_model->getAll('menu','order_id','ASC');

        if(empty($_POST)){
             if ($data['result'] === false)
                $this->session->set_flashdata('error_message', 'No data Found');
            $this->load->view('admin/menu/submenu-edit',$data);
        }else{
            $input['menu_id'] = $this->input->post('menu_id'); 
            $input['name'] = $this->input->post('name');      
            $input['order_id'] = $this->input->post('order_id');          
            $input['modified_at'] = date('Y-m-d');

            $this->Post_model->update('submenu',$input,$id);
            $this->session->set_flashdata('success_message', 'Data updated successfully');
            redirect('admin/menu/menu_sub_view');
        }

    }



    public function menu_sub_delete($id){
        $this->Post_model->delete('submenu',$id);

        $this->session->set_flashdata('success_message', 'Data Deleted successfully');
        redirect('admin/menu/menu_sub_view');
    }


/*---------------Submenu-------------*/


}

?>