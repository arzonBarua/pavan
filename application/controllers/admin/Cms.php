<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cms extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if(!($this->session->userdata('isLoged')) ){
            redirect('admin/index');
        }

        /*For ACL*/
        if($this->session->userdata('user_type')!='1'){
            $this->load->library('permission');
            if (!$this->permission->check_moudel()) {
                redirect('admin/home/deny_page');
            }
        }
        /*For ACL*/
        $this->load->library('form_validation');
        $this->load->model('Post_model');

    }

    public function slugify($text)
        {
          // replace non letter or digits by -
          $text = preg_replace('~[^\pL\d]+~u', '-', $text);

          // transliterate
          $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

          // remove unwanted characters
          $text = preg_replace('~[^-\w]+~', '', $text);

          // trim
          $text = trim($text, '-');

          // remove duplicate -
          $text = preg_replace('~-+~', '-', $text);

          // lowercase
          $text = strtolower($text);

          if (empty($text)) {
            return 'n-a';
          }

          return $text;
        }


    public function cms_add(){
        $data['menu_title'] = "Create Page";
        $data['menu_list'] = $this->Post_model->getAll('menu','order_id','ASC');

        if(empty($_POST)){
            $this->load->view('admin/cms/cms-add',$data);
        }else{  
            $file_upload = 1;
            $lastMenu = explode("_",$this->input->post('menu_id'));
            $input['menu_id'] = $lastMenu[0];

            $input['url'] = $this->slugify($lastMenu[1]);

            if($this->input->post('submenu_id')){
               $input['submenu_id'] = $this->input->post('submenu_id');
               $getSubmenuQeury = $this->Post_model->getAll('submenu','','',$input['submenu_id']);
               $input['url'] = $this->slugify($getSubmenuQeury[0]->name);
            }

            $input['customurl'] = $this->input->post('customurl');
            $input['content'] = $this->input->post('content');
            $input['styletype'] = $this->input->post('styletype');
            $input['scripttype'] = $this->input->post('scripttype');
            $input['metakeyword'] = $this->input->post('metakeyword');
            $input['metadescp'] = $this->input->post('metadescp');
            $input['status'] = $this->input->post('status');  

            if(!empty($_FILES['header_image']['name'])){

                $file_name = 'header_image';
                $upload_path = './assets/upload/header_image/';
                $result = $this->Common_operation->image_upload($file_name, $upload_path);


                if(!$result['flag']){
                    $data['error'] = $result['error'];
                    $this->load->view('admin/cms/cms-add', $data);
                    $file_upload = 0;
                }else{
                    $input['header_image'] = $result['picture'];

                }
            }

            $input['created_at'] = date("Y-m-d");

            if($file_upload){
                if ($this->Post_model->insert('cms',$input)) {
                        $this->session->set_flashdata('success_message', 'Data inserted successfully');
                        redirect('admin/cms/cms_view');
                    } else {
                        $data['error'] = "somehting went wrong please inform the webmaster";
                        $this->load->view('admin/cms/cms-add', $data);
                }
            }

        }    
    }


    public function cms_edit($id){
         $data['menu_title'] = "Edit Create Page";
         $data['menu_list'] = $this->Post_model->getAll('menu','order_id','ASC');

         $data['result'] = $this->Post_model->getAll('cms','','',$id);


         if($data['result'][0]->submenu_id != 0){
            $data['get_submenu'] = $this->Post_model->getAll('submenu','','',$data['result'][0]->submenu_id);
         }

        if(empty($_POST)){
            if ($data['result'] === false)
                $this->session->set_flashdata('error_message', 'No data Found');
            $this->load->view('admin/cms/cms-edit',$data);
        }else{

                $file_upload = 1;
                $lastMenu = explode("_",$this->input->post('menu_id'));
                $input['menu_id'] = $lastMenu[0];

                $input['url'] = $this->slugify($lastMenu[1]);

                if($this->input->post('submenu_id')){
                   $input['submenu_id'] = $this->input->post('submenu_id');
                   $getSubmenuQeury = $this->Post_model->getAll('submenu','','',$input['submenu_id']);
                   $input['url'] = $this->slugify($getSubmenuQeury[0]->name);
                }else{
                    $input['submenu_id'] = 0;
                }

                $input['customurl'] = $this->input->post('customurl');
                $input['content'] = $this->input->post('content');
                $input['styletype'] = $this->input->post('styletype');
                $input['scripttype'] = $this->input->post('scripttype');
                $input['metakeyword'] = $this->input->post('metakeyword');
                $input['metadescp'] = $this->input->post('metadescp');
                $input['status'] = $this->input->post('status'); 
                $input['updated_at'] = date("Y-m-d");


                if(!empty($_FILES['header_image']['name'])){
                    $file_name = 'header_image';
                    $upload_path = './assets/upload/header_image/';

                    $file_name_folder = (string)$data['result'][0]->header_image;
                    $full_path = $upload_path.$file_name_folder;

                    if (file_exists($full_path)) {
                        $this->Common_operation->file_delete($full_path);
                    }

                    $result = $this->Common_operation->image_upload($file_name, $upload_path);

                    if(!$result['flag']){
                        $data['error'] = $result['error'];
                        $this->load->view('admin/cms/cms-edit', $data);
                        $file_upload = 0;
                    }else{
                        $input['header_image'] = $result['picture'];
                    }
                }

                if ($this->Post_model->update('cms',$input,$id)){
                    $this->session->set_flashdata('success_message', 'Data updated successfully');
                    redirect('admin/cms/cms_view');
                }else{
                    $data['error'] = "somehting went wrong please inform the webmaster";
                    $this->load->view('admin/cms/cms-edit', $data);
                }
          
        }
    }


    public function cms_view(){
        $data['menu_title'] = "CMS View";
        //$data['result'] = $this->Post_model->getAll('cms','','DESC');

         $data['result'] = $this->Post_model->custom_query("Select menu.name as menu_name, submenu.name as submenu_name, cms.* from cms left join menu on cms.menu_id =  menu.id left join submenu on cms.submenu_id = submenu.id order by cms.id ASC");

        if($data['result'] === false)
            $this->session->set_flashdata('error_message', 'No data Found');

        $this->load->view('admin/cms/cms-view', $data);
    }

    public function cms_delete($id){
        $result = $this->Post_model->getAll('cms', '', '', $id);
        $file_name = (string)$result[0]->header_image;
       

        if(!empty($file_name)){
            $upload_path = './assets/upload/header_image/'.$file_name;
            $this->Post_model->delete('cms', $id, $upload_path);
        }

        $this->session->set_flashdata('success_message', 'Data Deleted successfully');
        redirect('admin/cms/cms_view');
    }




}

?>