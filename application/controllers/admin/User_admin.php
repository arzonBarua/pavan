<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: arzon
 * Date: 11/15/2016
 * Time: 10:40 AM
 */
class User_admin extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        if(!($this->session->userdata('isLoged'))){
            redirect('admin/index');
        }

        /*For ACL*/
        if($this->session->userdata('user_type')!='1'){
            $this->load->library('permission');
            if (!$this->permission->check_moudel()) {
                redirect('admin/home/deny_page');
            }
        }
        /*For ACL*/
        $this->Menu_model->menu_model_info();
        $this->load->library('form_validation');
        $this->load->helper('file');
        $this->load->model('User_model');
        $this->load->model('Post_model');
    }

    public function user_add(){
        $data['menu_title'] = "User Add";
        $data['type_list'] = $this->Post_model->getAll('admin_type','id');


        if(empty($_POST)) {
            $this->load->view('admin/user/user-add', $data);
        }else{
            $this->form_validation->set_rules('full_name','Full Name','required');
            $this->form_validation->set_rules('user_type','User Type','required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_emailExisting');
            $this->form_validation->set_rules('password', 'Password', 'required|callback_passwordCheck');
            $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');

            if($this->form_validation->run() == FALSE ){
                $this->load->view('admin/user/user-add', $data);
            }else{

                $input['full_name'] = $this->input->post('full_name');
                $input['department'] = $this->input->post('department');
                $input['designation'] = $this->input->post('designation');
                $input['user_type'] = $this->input->post('user_type');
                $input['email'] = $this->input->post('email');

                if($this->input->post('ip_restriction')) {
                    $input['ip_restriction'] = $this->input->post('ip_restriction');
                }

                $input['password'] = md5($this->input->post('password'));
                $input['ip_address'] = $this->input->post('ip_address');
                $input['created_at'] = date('Y-m-d');

               /*For File Upload*/
                $file_upload = 1;
                if(!empty($_FILES['userfile']['name'])){
                    $file_name = 'userfile';
                    $upload_path = './assets/upload/profile/';
                    $result = $this->Common_operation->image_upload($file_name,$upload_path);

                    if(!$result['flag']){
                        $data['error'] = $result['error'];
                        $this->load->view('admin/user/user-add', $data);
                        $file_upload = 0;
                    }else{
                        $input['picture'] = $result['picture'];
                    }
                }
                /*For File Upload*/
                if($file_upload){
                    if ($this->Post_model->insert('admin_login',$input)) {
                        $this->session->set_flashdata('success_message', 'Data inserted successfully');
                        redirect('admin/user_admin/user_view');
                    } else {
                        $data = array('error' => "somehting went wrong please inform the webmaster");
                        $this->load->view('admin/user/user-add', $data);
                    }
                }

            }
        }
    }

    public function user_edit($id){
        $data['menu_title'] = "User Edit";
        $data['type_list'] = $this->Post_model->getAll('admin_type','id');



        $data['result'] = $this->Post_model->getAll('admin_login','','',$id);


        if(empty($_POST)){
            if ($data['result'] === false)
                $this->session->set_flashdata('error_message', 'No data Found');
            $this->load->view('admin/user/user-edit',$data);
        }else{
            $this->form_validation->set_rules('full_name','Full Name','required');
            $this->form_validation->set_rules('user_type','User Type','required');

            if((string)$data['result'][0]->email != $this->input->post('email')){
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_emailExisting');
            }

            if($this->input->post('password')){
                $this->form_validation->set_rules('password', 'Password', 'required|callback_passwordCheck');
                $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');
            }

            if($this->form_validation->run() == FALSE){
                $this->load->view('admin/user/user-edit', $data);
            }else{

                $input['full_name'] = $this->input->post('full_name');
                $input['department'] = $this->input->post('department');
                $input['designation'] = $this->input->post('designation');
                $input['user_type'] = $this->input->post('user_type');
                $input['email'] = $this->input->post('email');

                if($this->input->post('password')){
                    $input['password'] = md5($this->input->post('password'));
                }

                $input['ip_restriction'] = $this->input->post('ip_restriction');

                $input['ip_address'] = $this->input->post('ip_address');
                $input['updated_at'] = date('Y-m-d');

                        /*For File Upload*/
                        $file_upload = 1;
                        if(!empty($_FILES['userfile']['name'])){
                            $file_name = 'userfile';
                            $upload_path = './assets/upload/profile/';

                            $file_name_folder = (string)$data['result'][0]->picture;
                            $full_path = $upload_path.$file_name_folder;

                            //delete files
                            if (file_exists($full_path) && !empty($file_name_folder)) {
                                $this->Common_operation->file_delete($full_path);
                            }

                            //delete files

                            $result = $this->Common_operation->image_upload($file_name,$upload_path);

                            if(!$result['flag']){
                                $data['error'] = $result['error'];
                                $this->load->view('admin/user/user-edit', $data);
                                $file_upload = 0;
                            }else{
                                $input['picture'] = $result['picture'];
                            }
                        }
                        /*For File Upload*/
                        if($file_upload){
                            if ($this->Post_model->update('admin_login',$input,$id)){
                                $this->session->set_flashdata('success_message', 'Data updated successfully');
                                redirect('admin/user_admin/user_view');
                            }else{
                                $data = array('error' => "somehting went wrong please inform the webmaster");
                                $this->load->view('admin/user/user-edit', $data);
                            }
                        }
            }


        }

    }


    public function user_view(){
        $data['menu_title'] = "User View";
        $data['result'] = $this->User_model->get_user_all_type();
        if($data['result']==0)
            $this->session->set_flashdata('error_message', 'No data Found');

        $this->load->view('admin/user/user-view',$data);
    }

    public function user_delete($id){
        $result = $this->Post_model->getAll('admin_login','','',$id);
        $file_name = (string)$result[0]->picture;

        if(!empty($file_name)){
            $upload_path = './assets/upload/profile/'.$file_name;
            $this->Post_model->delete('admin_login',$id,$upload_path);
        }else{
            $this->Post_model->delete('admin_login',$id);
        }

        $this->session->set_flashdata('success_message', 'Data Deleted successfully');
        redirect('admin/user_admin/user_view');
    }



    public function passwordCheck($lgn_pwd){
        if(strlen($lgn_pwd) > 5){
            return TRUE;
        }else{
            $this->form_validation->set_message('passwordCheck', 'Password length must be 5');
            return FALSE;
        }

    }

    public function emailExisting($email){

        if($this->User_model->check_email($email)){
            return TRUE;
        }else{
            $this->form_validation->set_message('emailExisting', 'Email Already Exists');
            return FALSE;
        }

    }



}