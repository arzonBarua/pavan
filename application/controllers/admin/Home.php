<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if(!($this->session->userdata('isLoged'))){
            redirect('admin/index');
        }
        $this->Menu_model->menu_model_info();


        $this->load->library('form_validation');
        $this->load->helper('file');
        $this->load->model('User_model');
        $this->load->model('Post_model');
    }

    public function index()
    {
        $data['menu_title'] = "Dashboard";
        $this->load->view('admin/index',$data);
    }


    public function deny_page(){
        $data['menu_title'] = "Permission";
        $this->load->view('admin/deny',$data);
    }

    public function user_profile(){
        $id = $this->session->userdata('id');
        $data['menu_title'] = "User Profile";
        $data['type_list'] = $this->Post_model->getAll('admin_type','id');
        $data['result'] = $this->Post_model->getAll('admin_login','','',$id);

        if(empty($_POST)){
            if($data['result']==0)
                $this->session->set_flashdata('error_message', 'No data Found');
            $this->load->view('admin/user/user-profile', $data);
        }else{

            $input['full_name'] = $this->input->post('full_name');
            $input['department'] = $this->input->post('department');
            $input['designation'] = $this->input->post('designation');
            $input['email'] = $this->input->post('email');

            if($this->input->post('password')){
                $input['password'] = md5($this->input->post('password'));
            }

            $input['updated_at'] = date('Y-m-d');


            /*For File Upload*/
            $file_upload = 1;
            if(!empty($_FILES['userfile']['name'])){

                $file_name = 'userfile';
                $upload_path = './assets/upload/profile/';

                $file_name_folder = (string)$data['result'][0]->picture;
                $full_path = $upload_path.$file_name_folder;

                //delete files

                if (file_exists($full_path)) {
                    $this->Common_operation->file_delete($full_path);
                }

                //delete files

                $result = $this->Common_operation->image_upload($file_name,$upload_path);

                if(!$result['flag']){
                    $data['error'] = $result['error'];
                    $this->load->view('admin/user/user-profile', $data);
                    $file_upload = 0;
                }else{
                    $input['picture'] = $result['picture'];
                }
            }
            /*For File Upload*/
            if($file_upload){
                if ($this->Post_model->update('admin_login',$input,$id)){
                    $this->session->set_flashdata('success_message', 'Data updated successfully');
                    redirect('admin/home/user_profile');
                }else{
                    $data = array('error' => "somehting went wrong please inform the webmaster");
                    $this->load->view('admin/user/user-profile', $data);
                }
            }

        }

    }



    public function logout(){
        $this->session->unset_userdata('temp_id');
        $this->session->unset_userdata('temp_parent_id');

        $this->session->unset_userdata('picture');
        $this->session->unset_userdata('full_name');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('designation');
        $this->session->unset_userdata('created_at');
        $this->session->unset_userdata('user_type');
        $this->session->unset_userdata('access');
        $this->session->unset_userdata('isLoged');
        $this->session->set_flashdata('logged_out', 'You have been logged out');

        redirect('admin/index');
    }


}
