<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends CI_Controller {
    public function __construct(){
        parent::__construct();
        if(!($this->session->userdata('isLoged')) ){
            redirect('admin/index');
        }

        /*For ACL*/
        if($this->session->userdata('user_type')!='1'){
            $this->load->library('permission');
            if (!$this->permission->check_moudel()) {
                redirect('admin/home/deny_page');
            }
        }
        /*For ACL*/
        $this->load->library('form_validation');
        $this->load->model('Post_model');
    }

     public function content_edit($type,$id){
        $data['menu_title'] = "Website Content";
        $data['label'] = ($id==1) ? 'Index Content' : '';
        
        $data['type'] = $type;

        $data['result'] = $this->Post_model->getAll('website_content','','',$id);

        if(empty($_POST)){
            if ($data['result'] === false)
                $this->session->set_flashdata('error_message', 'No data Found');
            $this->load->view('admin/content/content-edit',$data);
        }else{
            $this->form_validation->set_rules('content','content','required');

            if($this->form_validation->run() == FALSE){
                $this->load->view('admin/cms/cms-edit', $data);
            }else{

                $input['content'] = $this->input->post('content');
                $input['updated_at'] = date('Y-m-d');

                if ($this->Post_model->update('website_content',$input,$id)){
                    $this->session->set_flashdata('success_message', 'Data updated successfully');
                    redirect('admin/content/content_edit/'.$type.'/'.$id);
                }else{
                    $data = array('error' => "somehting went wrong please inform the webmaster");
                    $this->load->view('admin/cms/cms-edit', $data);
                }
            }
        }
    }

    public function contact_edit(){
        $data['menu_title'] = "Contact";
        $data['result'] = $this->Post_model->getAll('contact','','','1');

        if(empty($_POST)){
            $this->load->view('admin/content/contact-edit',$data);
        }else{
            $input['contact_number'] = $this->input->post('contact_number');
            $input['email'] =  $this->input->post('email');
            $input['address'] = $this->input->post('address');
            $input['updated_at'] = date('Y-m-d');


            if ($this->Post_model->update('contact',$input,'1')){
                $this->session->set_flashdata('success_message', 'Data updated successfully');
                redirect('admin/content/contact_edit');
            }else{
                $data['error'] = "somehting went wrong please inform the webmaster";
                $this->load->view('admin/content/contact-edit', $data);
            }

        }
    }

    public function quote_add(){
        $data['menu_title'] = "Quote";

        if(empty($_POST)){
            $this->load->view('admin/content/quote-add',$data);
        }else{
            $input['name'] = $this->input->post('name');
            $input['designation'] =  $this->input->post('designation');
            $input['quote'] = $this->input->post('quote');
            $input['status'] = $this->input->post('status');
            $input['created_at'] = date('Y-m-d');

            if ($this->Post_model->insert('quote',$input)){
                $this->session->set_flashdata('success_message', 'Data updated successfully');
                redirect('admin/content/quote_view');
            }else{
                $data['error'] = "somehting went wrong please inform the webmaster";
                $this->load->view('admin/content/quote-add', $data);
            }

        }
    }

    public function quote_edit($id){
         $data['menu_title'] = "Quote";
         $data['result'] = $this->Post_model->getAll('quote','','',$id);

        if(empty($_POST)){
            $this->load->view('admin/content/quote-edit',$data);
        }else{
            $input['name'] = $this->input->post('name');
            $input['designation'] =  $this->input->post('designation');
            $input['quote'] = $this->input->post('quote');
            $input['status'] = $this->input->post('status');
            $input['created_at'] = date('Y-m-d');

            if ($this->Post_model->update('quote',$input,$id)){
                $this->session->set_flashdata('success_message', 'Data updated successfully');
                redirect('admin/content/quote_view');
            }else{
                $data['error'] = "somehting went wrong please inform the webmaster";
                $this->load->view('admin/content/quote-edit', $data);
            }
        }
    }

    public function quote_view(){
        $data['menu_title'] = "Quote";
        $data['result'] = $this->Post_model->getAll('quote','id','ASC');

        if($data['result'] === false)
            $this->session->set_flashdata('error_message', 'No data Found');

        $this->load->view('admin/content/quote-view', $data);
    }


    public function quote_delete($id){
        $this->Post_model->delete('quote', $id);
        
        $this->session->set_flashdata('success_message', 'Data Deleted successfully');
        redirect('admin/content/quote_view');
    }

    public function image_info($id=null){
        $data['menu_title'] = "Index Image";

        $data['result'] = $this->Post_model->getAll('index_image','id','ASC');

        if(!empty($id)){
            $input['status'] = 1;
            $this->db->query("update index_image set status=0");
            $this->Post_model->update('index_image', $input, $id);
            $data['result'] = $this->Post_model->getAll('index_image','id','ASC');
            $this->load->view('admin/content/image-info', $data);
        }elseif(empty($_FILES)) {
            $this->load->view('admin/content/image-info', $data);
        }else{
            $file_upload = 1;

            if(!empty($_FILES['index_image']['name'])){
                $file_name = 'index_image';
                $upload_path = './assets/upload/index_image/';
                $result = $this->Common_operation->image_upload($file_name, $upload_path);

                if(!$result['flag']){
                    $data['error'] = $result['error'];
                    $this->load->view('admin/content/image-info', $data);
                    $file_upload = 0;
                }else{
                    $input['index_image'] = $result['picture'];
                }
            }else{
                $file_upload = 0;
            }

            if($file_upload) {
                if ($this->Post_model->insert('index_image', $input)) {
                    $this->session->set_flashdata('success_message', 'Data updated successfully');
                    redirect('admin/content/image_info');
                } else {
                    $data['error'] = "somehting went wrong please inform the webmaster";
                    $this->load->view('admin/content/image-info', $data);
                }
            }

        }
    }

    public function image_delete($id){
        $result = $this->Post_model->getAll('index_image', '', '', $id);
        $file_name = (string)$result[0]->index_image;


        if(!empty($file_name)){
            $upload_path = './assets/upload/index_image/'.$file_name;
            $this->Post_model->delete('index_image', $id, $upload_path);
        }

        $this->session->set_flashdata('success_message', 'Data Deleted successfully');
        redirect('admin/content/image_info');
    }


    public function news_add(){
        $data['menu_title'] = "News";

        if(empty($_POST)){
            $this->load->view('admin/content/news-add',$data);
        }else{
            $file_upload = 1;


            $input['news'] = $this->input->post('news');
            $input['title'] = $this->input->post('title');
            $input['news'] = $this->input->post('news');
            $input['publish_date'] = date('Y-m-d',strtotime($this->input->post('publish_date')));
            $input['status'] = $this->input->post('status');


            if(!empty($_FILES['feature_image']['name'])){

                $file_name = 'feature_image';
                $upload_path = './assets/upload/feature_image/';
                $result = $this->Common_operation->image_upload($file_name, $upload_path);


                if(!$result['flag']){
                    $data['error'] = $result['error'];
                    $this->load->view('admin/content/news-add', $data);
                    $file_upload = 0;
                }else{
                    $input['feature_image'] = $result['picture'];

                }
            }

            $input['created_at'] = date("Y-m-d");

            if($file_upload){
                if ($this->Post_model->insert('news',$input)) {
                    $this->session->set_flashdata('success_message', 'Data inserted successfully');
                    redirect('admin/content/news_view');
                } else {
                    $data['error'] = "somehting went wrong please inform the webmaster";
                    $this->load->view('admin/content/news-add', $data);
                }
            }

        }
    }


    public function news_view(){
        $data['menu_title'] = "News View";
        $data['result'] = $this->Post_model->getAll('news','id','ASC');

        if($data['result'] === false)
            $this->session->set_flashdata('error_message', 'No data Found');

        $this->load->view('admin/content/news-view', $data);
    }

    public function news_edit($id){
        $data['menu_title'] = "Edit News";
        $data['menu_list'] = $this->Post_model->getAll('news','id','ASC');

        $data['result'] = $this->Post_model->getAll('news','','',$id);


        if(empty($_POST)){
            if ($data['result'] === false)
                $this->session->set_flashdata('error_message', 'No data Found');
            $this->load->view('admin/content/news-edit',$data);
        }else{

            $file_upload = 1;


            $input['news'] = $this->input->post('news');
            $input['title'] = $this->input->post('title');
            $input['news'] = $this->input->post('news');
            $input['publish_date'] = date('Y-m-d',strtotime($this->input->post('publish_date')));
            $input['status'] = $this->input->post('status');


            if(!empty($_FILES['feature_image']['name'])){
                $file_name = 'feature_image';
                $upload_path = './assets/upload/feature_image/';

                $file_name_folder = (string)$data['result'][0]->feature_image;
                $full_path = $upload_path.$file_name_folder;

                if (file_exists($full_path)) {
                    $this->Common_operation->file_delete($full_path);
                }

                $result = $this->Common_operation->image_upload($file_name, $upload_path);

                if(!$result['flag']){
                    $data['error'] = $result['error'];
                    $this->load->view('admin/content/news-edit', $data);
                    $file_upload = 0;
                }else{
                    $input['feature_image'] = $result['picture'];
                }
            }

            if ($this->Post_model->update('news',$input,$id)){
                $this->session->set_flashdata('success_message', 'Data updated successfully');
                redirect('admin/content/news_view');
            }else{
                $data['error'] = "somehting went wrong please inform the webmaster";
                $this->load->view('admin/content/news-edit', $data);
            }

        }
    }

    public function news_delete($id){
        $result = $this->Post_model->getAll('news', '', '', $id);
        $file_name = (string)$result[0]->feature_image;

        if(!empty($file_name)){
            $upload_path = './assets/upload/feature_image/'.$file_name;
            $this->Post_model->delete('news', $id, $upload_path);
        }

        $this->session->set_flashdata('success_message', 'Data Deleted successfully');
        redirect('admin/content/news_view');
    }



//    public function index_image(){
//        $data['menu_title'] = "Index Image";
//          $this->load->view('admin/content/index-image',$data);
//
//        if(empty($_POST)) {
//            $this->load->view('admin/content/index-image', $data);
//        }else{
//            $file_upload = 1;
//            if(!empty($_FILES['index_image']['name'])){
//                $file_name = 'index_image';
//                $upload_path = './assets/upload/index_image/';
//                $result = $this->Common_operation->image_upload($file_name, $upload_path);
//
//                if(!$result['flag']){
//                    $data['error'] = $result['error'];
//                    $this->load->view('admin/content/index-image', $data);
//                    $file_upload = 0;
//                }else{
//                    $input['index_image'] = $result['picture'];
//                }
//            }else{
//                $file_upload = 0;
//            }
//
//            if($file_upload) {
//                if ($this->Post_model->insert('index_image', $input)) {
//                    $this->session->set_flashdata('success_message', 'Data updated successfully');
//                    redirect('admin/content/index-image');
//                } else {
//                    $data['error'] = "somehting went wrong please inform the webmaster";
//                    $this->load->view('admin/content/index-image', $data);
//                }
//            }
//
//        }
//    }

    public function email($id){
        $data['menu_title'] = "Website Content";
        $data['result'] = $this->Post_model->getAll('email_body','','',$id);

        if(empty($_POST)){
            $this->load->view('admin/content/email-body',$data);
        }else{
          
            $input['from'] = $this->input->post('from');
            $input['name'] = $this->input->post('name');
            $input['subject'] = $this->input->post('subject');
            $input['message'] = $this->input->post('message');
            $input['updated_at'] = date('Y-m-d');
            if ($this->Post_model->update('email_body',$input,$id)){
                $this->session->set_flashdata('success_message', 'Data updated successfully');
                redirect('admin/content/email/'.$id);
            }else{
                $data['error'] = "somehting went wrong please inform the webmaster";
                $this->load->view('admin/content/email/'.$id, $data);
            }

        }


    }

}