<?php defined('BASEPATH') OR exit('No direct script access allowed');


class User extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if($this->session->userdata('isLoged')){
            redirect('admin/home');
        }

        /*---Captcha setup---*/
        include APPPATH . 'third_party/captcha/autoload.php';
        include APPPATH . 'third_party/captcha/captcha.php';
        /*---End Captcha Setup---*/

        $this->load->helper('cookie');
        $this->load->model('User_model');
        $this->load->library('form_validation');
    }

    public function index(){
        $data = $this->check_cookie();

        /*--Error Check--*/
        $error = $this->session->userdata('error_count');
        if($error >= 3) {
            $data['captcha'] = captcha::getsiteKey();
        }
        /*--Error Check--*/

        $this->load->view('admin/login/index',$data);
    }

    public function check_cookie(){
        $data = "";
        if(get_cookie('login_info')){
            $result = get_cookie('login_info');
            $result_devide = explode(",",$result);
            $data['email'] = $result_devide[0];
            $data['password'] = $result_devide[1];
        }
        return $data;
    }

    public function form_process(){
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('login_failed', 'Sorry, Please fill all the fields or mail address is not valid');
            redirect('admin/index');
        } else {
                $email = $this->input->post('email');
                $password = $this->input->post('password');

                $data = $this->check_cookie();

                if (!empty($data)) {
                    if ($password != $data['password']) {
                        $password = md5($password);
                    }
                } else {
                    $password = md5($password);
                }

                $checkBox = $this->input->post("checkbox");
                if (!empty($checkBox) && ($checkBox == "on")) {
                    $cookie = array(
                        'name' => 'login_info',
                        'value' => "$email,$password",
                        'expire' => 3600 * 24 * 30, //For 30 days
                        'path' => '/',
                    );
                    $this->input->set_cookie($cookie);
                } else {
                    delete_cookie('login_info');
                }

              
                    $user_id = $this->User_model->login_user($email, $password);

                    if ($user_id) {
                        redirect('admin/home/index');
                    } else {
                        /*--Error Count--*/
                        $error_count = 1;
                        if ($this->session->userdata('error_count')) {
                            $error_count = $this->session->userdata('error_count');
                            $error_count += 1;
                        }
                        $user_data = array(
                            'error_count' => $error_count,
                        );
                        $this->session->set_userdata($user_data);
                        /*--Error Count--*/

                        $this->session->set_flashdata('login_failed', 'Username or password did not match or <br> IP Adress is not allowed');
                        redirect('admin/index');
                    }
             
            }
    }
}