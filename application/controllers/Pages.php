<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('Post_model');
    }

    public function index(){
        $homrurl = $this->uri->segment(1);

        $get = $this->Post_model->getAllByFieldName('cms','customurl',$homrurl);

        $url = 'url';
        if($get) {
            $url = 'customurl';
        }

        $data['result'] = $this->Post_model->getAllByFieldName('cms',$url,$homrurl);



        $data['page'] = !empty($data['result'][0]->customurl) ? $data['result'][0]->customurl : $data['result'][0]->$url;
        $data['metakeyword'] =  $data['result'][0]->metakeyword;
        $data['metadescp'] =  $data['result'][0]->metadescp;
        $this->load->view('site/pages',$data);
    }

}