<?php

class User_model extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function login_user($email, $password){
        $sql = "select admin_login.id,ip_restriction,ip_address from admin_login where email=? and password=?";
        $query = $this->db->query($sql,array($email, $password));
        $result = $query->row();

        if($result){
            /*---check IP restriction---*/
                if($result->ip_restriction){
                    if($this->input->ip_address() != $result->ip_address){
                        return false;
                    }
                }
            /*---End check IP restriction---*/

            /*--- Check Error Count ---*/
            if($this->session->userdata('error_count')){
                $this->session->unset_userdata('error_count');
            }
            /*--- Check Error Count ---*/
                $user_data = array(
                    'id' => $result->id,
                    'isLoged' => true
                );

                $this->session->set_userdata($user_data);
                return true;
        }else{
            return false;
        }
    }

    public function get_user_all(){
        $sql = "select * from admin_login where user_type != '1' order by full_name asc";
        $query = $this->db->query($sql);

        if ($query->row()) {
            foreach ($query->result() as $row) {
                $userList[$row->id] = $row->full_name;
            }
            return $userList;
        }

        return 0;


    }

    public function get_user_all_type(){
        $sql = "select admin_login.id ,admin_login.full_name, admin_login.email, admin_login.department, admin_login.designation, admin_login.ip_address, admin_login.ip_address, admin_login.picture ,admin_login.created_at, admin_type.name from admin_login JOIN admin_type on admin_login.user_type = admin_type.id order by admin_login.full_name asc";
        $query = $this->db->query($sql);

        $result = $query->row();
        if($result){
            return $query->result();
        }
        return 0;
    }


    public function change_pass_status(){

    }

    public function check_email($email){
        $sql = "SELECT * FROM admin_login where email=?";
        $query = $this->db->query($sql,array($email));
        $result = $query->row();
        if($result){
            return false;
        }
        return true;
    }



}