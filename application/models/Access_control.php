<?php

/**
 * Created by PhpStorm.
 * User: arzon
 * Date: 11/13/2016
 * Time: 12:19 PM
 */
class Access_control extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }


    public function insert($input){
        if($this->db->insert("acl_management",$input)){
            return true;
        }
        return false;
    }

    public function update($input,$id)
    {
        $this->db->where('id',$id);
        $this->db->update('acl_management',$input);

        if($this->db->affected_rows() >=0){
            return true;
        }
        return false;
    }

    public function delete($id){
        $this->db->delete('acl_management', array('id' => $id));
    }

    public function check($type){
        $sql = "SELECT * FROM acl_management where login_id=?";
        $query = $this->db->query($sql,array($type));
        return $query->row();
    }

    public function get_acl($id){
        $sql = "SELECT * FROM acl_management where id=?";
        $query = $this->db->query($sql,array($id));
        return $query->result();
    }

    public function acl_list(){
        $sql = "SELECT admin_login.full_name, acl_management.id, acl_management.access  FROM admin_login JOIN acl_management on admin_login.id=acl_management.login_id where admin_login.user_type != '1'";
        $query = $this->db->query($sql);
        $result = $query->row();
        if($result){
            return $query->result();
        }
        return 0;
    }

}