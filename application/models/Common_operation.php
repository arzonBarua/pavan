<?php

/**
 * Created by PhpStorm.
 * User: arzon
 * Date: 11/15/2016
 * Time: 9:23 PM Common_operation
 */
class Common_operation extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Post_model');
    }

    public function image_upload($file_name,$upload_path,$min_width=null,$min_height=null, $max_width="1920", $max_height="1042",$crop_width=null,$crop_height=null){
        $data['flag'] = true;
        $config['upload_path'] = $upload_path; //full path
        $config['allowed_types'] = 'gif|jpg|png';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = 4096;
        $config['max_width'] = $max_width; //min_width
        $config['max_height'] = $max_height; //min_height

        if(!empty($min_width)){
            $config['min_width'] = $min_width;
        }

        if(!empty($min_height)){
            $config['min_height'] = $min_height;
        }


        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($file_name)) {
            $data['flag'] = false;
            $data['error'] = $this->upload->display_errors();
        }else {
            $image_data = $this->upload->data();
            $config['image_library'] = 'gd2';
            $config['source_image'] = $image_data['full_path']; //full path
            $config['new_image'] = $upload_path.'thumb/';
            $config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;

            $config['width'] = 125;
            $config['height'] = 100;

            if(!empty($crop_width)){
                $config['width'] = $crop_width;
            }

            if(!empty($crop_height)){
                $config['height'] = $crop_height;
            }


            $this->load->library('image_lib', $config);
            if (!$this->image_lib->resize()) {
                $data['flag'] = false;
                $data['error'] = $this->image_lib->display_errors();
            }
            $data['picture'] = $image_data["file_name"];
        }

        return $data;
    }

    public function doc_upload($file_name,$upload_path){
        $data['flag'] = true;
        $config['upload_path'] = $upload_path; //full path
        $config['allowed_types'] = 'pdf';
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($file_name)) {
            $data['flag'] = false;
            $data['error'] = $this->upload->display_errors();
        }else{
            $doc = $this->upload->data();
            $data['document'] = $doc["file_name"];
        }
        return $data;
    }

    public function file_delete($full_path,$type=null){
        unlink($full_path);
        $path_parts = pathinfo($full_path);
        $file_name_folder = $path_parts['filename'].'_thumb.'.$path_parts['extension'];

        if($type==null){
            $full_path = $path_parts['dirname'] . '/thumb/' . $file_name_folder;
            unlink($full_path);
        }

    }

    public function show_thumb($full_path){
        $path_parts = pathinfo($full_path);
        $file_name_folder = $path_parts['filename'].'_thumb.'.$path_parts['extension'];
        return $file_name_folder;
    }

    public function read_more($string, $size){
        $string = strip_tags($string);

        if (strlen($string) > $size) {
            // truncate string
            $stringCut = substr($string, 0, $size);

            // make sure it ends in a word so assassinate doesn't become ass...
            $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';
        }
        return $string;
    }


    public function footer_common_operation($id){
         $result =  $this->Post_model->getAllByFieldName('website_content','id',$id);
         echo $result[0]->content;
    }

    public function menu($id,$page=null){
        $data = array('menupos' => $id, 'status' => '1');
        $orWhere = array('menupos'=>'3');
        $order_field = ($id==1) ? 'position' : 'footer_position';
        $result = $this->Post_model->getAllMenuHeader('cms',$data,$order_field,'ASC',$orWhere);

        if($result) {
            $countMenu = 1;
            foreach ($result as $value) {

            	if((string)$value->status=='1'){

	                $url = !empty($value->customurl) ? $value->customurl : $value->url;
	                $active = '';
	                if(!empty($page)){
	                	if($url == $page){
	                	   $active = 'active';		
	                	}
	                }    
                    if($countMenu==4){
                        $showClass = ( ($this->uri->segment(1) == 'contact') ? 'active' : '' );
                        echo "<li class='$showClass'><a href='contact'>KUNDE WERDEN / ANGEBOT ANFORDERN</a></li>";
                    }
	                echo "<li class='$active'><a href='" .$url. "'>" . strtoupper($value->title) . "</a></li>";

                     $countMenu++;
	            }

            }
        }


    }

    public function tofloat($num) {
        $dotPos = strrpos($num, '.');
        $commaPos = strrpos($num, ',');
        $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos : 
            ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);
       
        if (!$sep) {
            return floatval(preg_replace("/[^0-9]/", "", $num));
        } 

        return floatval(
            preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
            preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
        );
    }

    public function send_mail($data){
        $config = array(
            'mailtype'  => 'html',
            'charset'   => 'utf-8'
        );

        // @$this->load->library('email',$config);

        // $this->email->clear();

        // $this->email->from($data['from'], $data['name']);
        // $this->email->to($data['email']);
        // $this->email->subject($data['subject']);
        // $this->email->message('test');
        // @$this->email->send();

        $this->load->library('email',$config);

        $this->email->from($data['from'], $data['name']);
        $this->email->to($data['email']);
       
        $this->email->subject($data['subject']);
        $this->email->message($data['message']);

        $this->email->send();
    }



}
