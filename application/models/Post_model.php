<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Post_model extends CI_Model
{
    private $table = "";

    function __construct()
    {
        parent::__construct();
    }

    function getAll($table, $field = 'id', $order = 'ASC', $id = null)
    {
		if( $id != null )
			$this->db->where('id', $id);
        $this->table = $table;
        $this->db->order_by($field, $order);
        $q = $this->db->get($this->table);
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

	function getAllByFieldName($table, $field_name, $field_val)
    {
        $this->db->where("`" . $field_name . "`", $field_val);
        $this->table = $table;
        $q = $this->db->get($this->table);


        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

   

    function getAjaxHTML($table, $field_name, $field_val)
    {
        $str = '<option value=""> - SELECT - </option>';
        $this->table = $table;
        $this->db->select('id, subcategory_name');
        $this->db->where("`" . $field_name . "`", $field_val);
        $q = $this->db->get($this->table);
        $str = '<option value="">--SELECT--</option>';
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $key => $value) {
                $str .= '<option value="' . $value->id . '">' . $value->subcategory_name . '</option>';
            }
            //return $str;
        }
        return $str;
    }

    function insert($table, $data)
    {
        $this->table = $table;
        if($this->db->insert($this->table, $data)){
            return true;
        }
        return false;
    }

    function update($table, $data, $id)
    {
        $this->table = $table;
        $this->db->where("id", $id);
        $this->db->update($this->table, $data);

        if($this->db->affected_rows() >= 0){
            return true;
        }

        return false;

    }

    function delete($table, $id, $path = null,$type=null)
    {
        $this->table = $table;
        $this->db->where("id", $id);
        $this->db->delete($this->table);

        if (!empty($path)) {
            $this->Common_operation->file_delete($path,$type);
        }
    }

    function join_table($select, $table, $join_id, $joinTable, $type, $field = null, $order = 'ASC')
    {
        $this->db->select($select);
        $this->db->from($table);

        for ($i = 0; $i < count($joinTable); $i++) {
            $string = $joinTable[$i]['name'] . ".id=" . $table . "." . $join_id[$i]['name']; //".".$join_id[$i]['name']."=".$joinTable[$i]['name'].".id";
            $this->db->join($joinTable[$i]['name'], $string, $type);
        }

        if (!empty($field))
            $this->db->order_by($field, $order);

        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;

    }

    public function custom_query($query_string){

        $q = $this->db->query($query_string);
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;

    }

}
