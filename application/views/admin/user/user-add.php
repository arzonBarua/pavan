<?php $this->load->view('./admin/header'); ?>

    <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">User Information</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php
                if(validation_errors() || isset($error)){
                    echo "<div class='alert alert-danger'>";
                    echo validation_errors();
                    echo isset($error) ? $error : "";
                    echo "</div>";
                }
                ?>
                <?php
                //$array = array('enctype'=>'multipart/form-data');
                echo form_open_multipart('admin/user_admin/user_add'); ?>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" name="full_name" class="form-control" id="Name" placeholder="Enter Name" autocomplete="off" value="<?php echo set_value('full_name'); ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Department</label>
                            <input type="text" name="department" class="form-control" id="Department" placeholder="Enter Department" value="<?php echo set_value('department'); ?>" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Designation</label>
                            <input type="text" name="designation" class="form-control" id="Designation" placeholder="Enter Designation" value="<?php echo set_value('designation'); ?>" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label>User Type</label>
                            <select name="user_type" class="form-control" required>
                                <option>--Select Type--</option>
                                <?php foreach($type_list as $type): ?>
                                <option value="<?php echo $type->id ?>" <?php echo (set_value('user_type')==$type->id) ? "selected" : "" ?> ><?php echo $type->name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" value="<?php echo set_value('email'); ?>" autocomplete="off" required>
                        </div>
                        <div class="form-group">
                            <label for="Password">Password</label>
                            <input type="password" name="password" class="form-control" id="Password" placeholder="Password" required>
                            <p>
                                [Password length must be five]
                            </p>
                        </div>
                        <div class="form-group">
                            <label for="passconf">Retype Password</label>
                            <input type="password" name="passconf" class="form-control" id="RetypePassword" placeholder="Password" required>

                        </div>



                        <div class="form-group">
                            <label for="userfile">File input</label>
                            <input type="file" name="userfile" id="userfile">
                            <p class="help-block">Example block-level help text here.</p>
                        </div>
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                <?php echo form_close(); ?>
            </div><!-- /.box -->







        </div>
    </div>

<?php $this->load->view('./admin/footer-link'); ?>

<script>
    $(function(){
       $('.ip_restriction').change(function(){
            if($(this).is(":checked")){
                var val = $(this).val();
                if(val==1){
                    $('#ip_address').show();
                    $('#ip_address_val').attr('required','required');
                }else{
                    $('#ip_address').hide();
                    $('#ip_address_val').removeAttr('required');
                    $('#ip_address_val').val("");
                }
            }
       });
    });
</script>

<?php $this->load->view('./admin/footer'); ?>