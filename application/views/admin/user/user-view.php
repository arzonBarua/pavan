<?php $this->load->view('./admin/header'); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Table User View</h3>
                </div><!-- /.box-header -->



                <?php if($result == 0){ ?>
                    <div class="alert alert-danger">
                        <?php echo $this->session->flashdata('error_message'); ?>
                    </div>
                <?php }else{ ?>

                    <?php if($this->session->flashdata('success_message')): ?>
                        <div class="alert alert-success">
                            <?php echo $this->session->flashdata('success_message'); ?>
                        </div>
                    <?php endif; ?>

                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="custom">SL</th>
                                <th>User Name</th>
                                <th>User Type</th>
                                <th>Email</th>
                                <th class="custom_last">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $sl = 1;
                            foreach($result as $value){ ?>
                                <tr>
                                    <td><?php echo $sl; ?></td>
                                    <td><?php echo $value->full_name; ?></td>
                                    <td><?php echo $value->name; ?></td>
                                    <td><?php echo $value->email ?></td>
                                    <td>
                                        <div class="modal fade" id="<?php echo $value->id ?>" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">View User</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table class="table table-hover table-nomargin table-bordered">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Full Name</td>
                                                                    <td align="center">
                                                                        <?php echo $value->full_name; ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>User Type</td>
                                                                    <td align="center">
                                                                        <?php echo $value->name; ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Email</td>
                                                                    <td align="center">
                                                                        <?php echo $value->email; ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Email</td>
                                                                    <td align="center">
                                                                        <?php echo $value->email; ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Department</td>
                                                                    <td align="center">
                                                                        <?php echo $value->department; ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Designation</td>
                                                                    <td align="center">
                                                                        <?php echo $value->designation; ?>
                                                                    </td>
                                                                </tr>
                                                               
                                                                <?php if(!empty($value->picture)): ?>
                                                                    <tr>
                                                                        <td>Picture</td>
                                                                        <td align="center">

                                                                            <img src="<?php echo base_url() ?>assets/upload/profile/thumb/<?php echo $this->Common_operation->show_thumb($value->picture); ?>">
                                                                        </td>
                                                                    </tr>
                                                                <?php endif; ?>


                                                            </tbody>
                                                        </table>


                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="javascript:void(0)" title="view" data-toggle="modal" data-target="#<?php echo $value->id ?>" ><i class="fa fa-fw fa-eye"></i></a> /
                                        <a href="user_edit/<?php echo $value->id ?>" title="Edit"><i class="fa fa-fw fa-edit"></i></a> /
                                        <a href="user_delete/<?php echo $value->id ?>" onclick="return confirm('Are you sure you wish to delete this ?')" title="Delete"><i class="fa fa-fw fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php $sl++; } ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                <?php } ?>
            </div><!-- /.box -->
        </div>
    </div>

<?php $this->load->view('./admin/footer-link') ?>

    <script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#example1").dataTable();
        });
    </script>


<?php $this->load->view('./admin/footer'); ?>