<?php $this->load->view('./admin/header'); ?>
<!--Editional Css-->

    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Contact Information</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php if($this->session->flashdata('success_message')): ?>
                    <div class="alert alert-success">
                        <?php echo $this->session->flashdata('success_message'); ?>
                    </div>
                <?php endif; ?>
                <?php


                if(validation_errors() || isset($error)){
                    echo "<div class='alert alert-danger'>";
                    echo validation_errors();
                    echo isset($error) ? $error : "";
                    echo "</div>";
                }
                ?>
                <?php
                //$array = array('enctype'=>'multipart/form-data');
                echo form_open_multipart('admin/content/quote_edit/'.$result[0]->id); ?>
                <div class="box-body">

                    <div class="form-group">  
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="name" autocomplete="off" value="<?php echo $result[0]->name ?>" required>
                    </div>

                    <div class="form-group">  
                            <label for="name">Designation</label>
                            <input type="text" name="designation" class="form-control" id="designation" placeholder="Designation" autocomplete="off" value="<?php echo $result[0]->designation ?>" required>
                    </div>


                    <div class="form-group">
                        <label for="content">Quote</label>
                        <textarea class="form-control" style="height: 150px;" name="quote" value="<?php echo $result[0]->quote ?>" rows="20" required><?php echo $result[0]->quote ?></textarea>
                    </div>

                     <div class="form-group">
                        <label for="ip_restriction">Status</label>
                        <div class="radio">
                            <label>
                                <input type="radio" name="status" id="status" class="status" value="1" <?php echo ($result[0]->status==1) ? 'checked' : ''; ?> required="">
                                Active
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="status" id="status" class="status" <?php echo ($result[0]->status==0) ? 'checked' : ''; ?> value="0" required="">
                                Inactive
                            </label>
                        </div>
                    </div>


                    <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                <?php echo form_close(); ?>
            </div><!-- /.box -->







        </div>
    </div>

<?php $this->load->view('./admin/footer-link'); ?>
  
<?php $this->load->view('./admin/footer'); ?>