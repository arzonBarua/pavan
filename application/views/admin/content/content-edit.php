<?php $this->load->view('./admin/header'); ?>
<!--Editional Css-->

    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Website Content Information</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php if($this->session->flashdata('success_message')): ?>
                    <div class="alert alert-success">
                        <?php echo $this->session->flashdata('success_message'); ?>
                    </div>
                <?php endif; ?>
                <?php


                if(validation_errors() || isset($error)){
                    echo "<div class='alert alert-danger'>";
                    echo validation_errors();
                    echo isset($error) ? $error : "";
                    echo "</div>";
                }
                ?>
                <?php
                //$array = array('enctype'=>'multipart/form-data');
                echo form_open_multipart('admin/content/content_edit/'.$type.'/'.$result[0]->id); ?>
                <div class="box-body">



                    <div class="form-group">
                        <label for="content"><?php echo $label ?></label>

                        <textarea class="form-control tinymce" style="height: 400px;" name="content" rows="20" required><?php echo (set_value('content') ? set_value('content') : $result[0]->content); ?></textarea>

                    </div>


                    <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                <?php echo form_close(); ?>
            </div><!-- /.box -->







        </div>
    </div>

<?php $this->load->view('./admin/footer-link'); ?>
  <script type="text/javascript" src="<?php print base_url();?>assets/js/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">

  //For Tinymce
            $('textarea.tinymce').tinymce({
                // Location of TinyMCE script
                script_url : '<?php print base_url();?>assets/js/tiny_mce/tiny_mce.js',

                // General options
                theme : "advanced",
                plugins : "jbimages,autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",
                valid_elements : '+*[*]',
                // Theme options
                theme_advanced_buttons1 : "jbimages,|,save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
                theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
                theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
                theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
                theme_advanced_toolbar_location : "top",
                theme_advanced_toolbar_align : "left",
                theme_advanced_statusbar_location : "bottom",
                theme_advanced_resizing : true,

                // ===========================================
                // Set RELATIVE_URLS to FALSE (This is required for images to display properly)
                // ===========================================

                relative_urls : false
            });
</script>



<?php $this->load->view('./admin/footer'); ?>