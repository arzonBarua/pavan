<?php $this->load->view('./admin/header'); ?>
    <style>
        .image-preview-input {
            position: relative;
            overflow: hidden;
            margin: 0px;
            color: #333;
            background-color: #fff;
            border-color: #ccc;
        }
        .image-preview-input input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }
        .image-preview-input-title {
            margin-left:2px;
        }
        .modal-content{
            background-color: transparent !important;
        }
    </style>
<!--Editional Css-->

    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Quote Information</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php if($this->session->flashdata('success_message')): ?>
                    <div class="alert alert-success">
                        <?php echo $this->session->flashdata('success_message'); ?>
                    </div>
                <?php endif; ?>
                <?php


                if(validation_errors() || isset($error)){
                    echo "<div class='alert alert-danger'>";
                    echo validation_errors();
                    echo isset($error) ? $error : "";
                    echo "</div>";
                }
                ?>
                <?php
                //$array = array('enctype'=>'multipart/form-data');
                echo form_open_multipart('admin/content/image_info'); ?>
                <div class="box-body">

                    <div class="form-group">
                        <label for="header_image">Header Image</label>
                        <!-- <input type="file" name="header_image" class="form-control" id="header_image" value="" autocomplete="off" required> -->
                        <div class="input-group image-preview">
                            <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                            <span class="input-group-btn">
                                    <!-- image-preview-clear button -->
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                        <span class="glyphicon glyphicon-remove"></span> Clear
                                    </button>
                                <!-- image-preview-input -->
                                    <div class="btn btn-default image-preview-input">
                                        <span class="glyphicon glyphicon-folder-open"></span>
                                        <span class="image-preview-input-title">Browse</span>
                                        <input type="file" accept="image/png, image/jpeg, image/gif" name="index_image"/> <!-- rename it -->
                                    </div>
                                </span>
                        </div>
                    </div>


                    <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                <?php echo form_close(); ?>
            </div><!-- /.box -->


            <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Table Index images</h3>
                            </div><!-- /.box-header -->

                            <?php if($result == 0){ ?>

                            <?php }else{ ?>



                                <div class="box-body">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th class="custom">SL</th>
                                            <th>Image</th>
                                            <th>Status</th>
                                            <th class="custom_last">Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php $sl = 1;
                                        $satus = array('0'=>'<span style="color:red">Inactive</span>','1'=>'<span style="color:green">Active</span>');

                                        foreach($result as $value){ ?>
                                            <tr>
                                                <td><?php echo $sl; ?></td>
                                                <td><?php $arr = explode(".",$value->index_image);
                                                $index_image = $arr[0]."_thumb.".$arr[1];
                                                ?>
                                                    <img src="<?php echo base_url()."assets/upload/index_image/thumb/".$index_image ?>"></td>
                                                <td><?php echo $satus[$value->status] ?></td>
                                                <td>
                                                    <div class="modal fade" id="<?php echo $value->id ?>" role="dialog">
                                                        <div class="modal-dialog">
                                                            <!-- Modal content-->
                                                            <div class="modal-content">

                                                                <div class="modal-body">
                                                                    <img src="<?php echo base_url()."assets/upload/index_image/".$value->index_image ?>" style="width:700px">
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="javascript:void(0)" title="view" data-toggle="modal" data-target="#<?php echo $value->id ?>" ><i class="fa fa-fw fa-eye"></i></a> /
                                                    <a href="<?php echo base_url()."admin/content/" ?>image_info/<?php echo $value->id ?>" title="swtich"><i class="fa fa-fw fa-recycle"></i></a> /
                                                    <a href="<?php echo base_url()."admin/content/" ?>image_delete/<?php echo $value->id ?>" onclick="return confirm('Are you sure you wish to delete this ?')" title="Delete"><i class="fa fa-fw fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                            <?php $sl++; } ?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                            <?php } ?>
                        </div><!-- /.box -->
                    </div>
                </div>
        </div>
    </div>

<?php $this->load->view('./admin/footer-link'); ?>
    <script type="text/javascript">
        $(document).on('click', '#close-preview', function(){
            $('.image-preview').popover('hide');
            // Hover befor close the preview
            $('.image-preview').hover(
                function () {
                    $('.image-preview').popover('show');
                },
                function () {
                    $('.image-preview').popover('hide');
                }
            );
        });

        $(function() {
            // Create the close button
            var closebtn = $('<button/>', {
                type:"button",
                text: 'x',
                id: 'close-preview',
                style: 'font-size: initial;',
            });
            closebtn.attr("class","close pull-right");
            // Set the popover default content
            $('.image-preview').popover({
                trigger:'manual',
                html:true,
                title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
                content: "There's no image",
                placement:'bottom'
            });
            // Clear event
            $('.image-preview-clear').click(function(){
                $('.image-preview').attr("data-content","").popover('hide');
                $('.image-preview-filename').val("");
                $('.image-preview-clear').hide();
                $('.image-preview-input input:file').val("");
                $(".image-preview-input-title").text("Browse");
            });
            // Create the preview image
            $(".image-preview-input input:file").change(function (){
                var img = $('<img/>', {
                    id: 'dynamic',
                    width:250,
                    height:200
                });
                var file = this.files[0];
                var reader = new FileReader();
                // Set preview image into the popover data-content
                reader.onload = function (e) {
                    $(".image-preview-input-title").text("Change");
                    $(".image-preview-clear").show();
                    $(".image-preview-filename").val(file.name);
                    img.attr('src', e.target.result);
                    $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
                reader.readAsDataURL(file);
            });
        });
    </script>
  
<?php $this->load->view('./admin/footer'); ?>