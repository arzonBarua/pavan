<?php $this->load->view('./admin/header'); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Table News View</h3>
                </div><!-- /.box-header -->

                <?php if($result == 0){ ?>
                    <div class="alert alert-danger">
                        <?php echo $this->session->flashdata('error_message'); ?>
                    </div>
                <?php }else{ ?>

                    <?php if($this->session->flashdata('success_message')): ?>
                        <div class="alert alert-success">
                            <?php echo $this->session->flashdata('success_message'); ?>
                        </div>
                    <?php endif; ?>

                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th class="custom">SL</th>
                                        <th>Title</th>
                                        <th>Publish Date</th>
                                        <th>Status</th>
                                        <th class="custom_last">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            <tbody>
                            <?php $sl = 1;
                            $satus = array('0'=>'<span style="color:red">Inactive</span>','1'=>'<span style="color:green">Active</span>');
                            foreach($result as $value){ ?>
                                <tr>
                                    <td><?php echo $sl; ?></td>
                                    <td><?php echo $value->title; ?></td>
                                    <td><?php echo date('d F Y',strtotime($value->publish_date)); ?></td>
                                    <td><?php echo $satus[$value->status] ?></td>
                                    <td>
                                        <div class="modal fade" id="<?php echo $value->id ?>" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">View CMS</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table class="table table-hover table-nomargin table-bordered">
                                                            <tbody>
                                                            <tr>
                                                                <td>Title</td>
                                                                <td align="center">
                                                                    <?php echo $value->title; ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Publish Date</td>
                                                                <td align="center">
                                                                    <?php echo date('d F Y',strtotime($value->publish_date)); ?>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>Feature Image</td>
                                                                <td align="center">
                                                                    <img src="<?php echo base_url()."assets/upload/feature_image/".$value->feature_image ?>" style="width:250px">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Status</td>
                                                                <td align="center">
                                                                    <?php echo $satus[$value->status]; ?>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>


                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="javascript:void(0)" title="view" data-toggle="modal" data-target="#<?php echo $value->id ?>" ><i class="fa fa-fw fa-eye"></i></a> /
                                        <a href="news_edit/<?php echo $value->id ?>" title="Edit"><i class="fa fa-fw fa-edit"></i></a> /
                                        <a href="news_delete/<?php echo $value->id ?>" onclick="return confirm('Are you sure you wish to delete this ?')" title="Delete"><i class="fa fa-fw fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php $sl++; } ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                <?php } ?>
            </div><!-- /.box -->
        </div>
    </div>

<?php $this->load->view('./admin/footer-link') ?>

    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#example1").DataTable();
        });
    </script>


<?php $this->load->view('./admin/footer'); ?>