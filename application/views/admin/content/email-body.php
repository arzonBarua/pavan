<?php $this->load->view('./admin/header'); ?>
<!--Editional Css-->

    <div class="row">
        <div class="col-md-8">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Email Body</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php if($this->session->flashdata('success_message')): ?>
                    <div class="alert alert-success">
                        <?php echo $this->session->flashdata('success_message'); ?>
                    </div>
                <?php endif; ?>
                <?php


                if(validation_errors() || isset($error)){
                    echo "<div class='alert alert-danger'>";
                    echo validation_errors();
                    echo isset($error) ? $error : "";
                    echo "</div>";
                }
                ?>
                <?php
                //$array = array('enctype'=>'multipart/form-data');
                echo form_open_multipart('admin/content/email/'.$result[0]->id); ?>
                <div class="box-body">
                     <div class="form-group">  
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="From" autocomplete="off" value="<?php echo $result[0]->name ?>" required>
                    </div>

                     <div class="form-group">  
                            <label for="from">From</label>
                            <input type="text" name="from" class="form-control" id="from" placeholder="From" autocomplete="off" value="<?php echo $result[0]->from ?>" required>
                    </div>


                    <div class="form-group">
                            <label for="subject">Subject</label>
                            <input type="text" name="subject" class="form-control" id="subject" placeholder="Subject" autocomplete="off" value="<?php echo $result[0]->subject ?>" required>
                    </div>

                    <div class="form-group">
                            <label for="message">Message</label>
                            <textarea class="form-control" name="message" placeholder=""><?php echo $result[0]->message ?></textarea>
                    </div>
                    <!-- /.box-body -->

                <div class="box-footer">
                   
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                <?php echo form_close(); ?>
            </div><!-- /.box -->

        </div>
    </div>

<?php $this->load->view('./admin/footer-link'); ?>
<?php $this->load->view('./admin/footer'); ?>