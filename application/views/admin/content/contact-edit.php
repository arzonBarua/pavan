<?php $this->load->view('./admin/header'); ?>
<!--Editional Css-->

    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Contact Information</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php if($this->session->flashdata('success_message')): ?>
                    <div class="alert alert-success">
                        <?php echo $this->session->flashdata('success_message'); ?>
                    </div>
                <?php endif; ?>
                <?php


                if(validation_errors() || isset($error)){
                    echo "<div class='alert alert-danger'>";
                    echo validation_errors();
                    echo isset($error) ? $error : "";
                    echo "</div>";
                }
                ?>
                <?php
                //$array = array('enctype'=>'multipart/form-data');
                echo form_open_multipart('admin/content/contact_edit'); ?>
                <div class="box-body">

                    <div class="form-group">  
                            <label for="name">Contact Number</label>
                            <input type="text" name="contact_number" class="form-control" id="contact_number" placeholder="Contact Number" autocomplete="off" value="<?php echo $result[0]->contact_number ?>" required>
                    </div>

                    <div class="form-group">  
                            <label for="name">Email</label>
                            <input type="text" name="email" class="form-control" id="email" placeholder="From" autocomplete="off" value="<?php echo $result[0]->email ?>" required>
                    </div>


                    <div class="form-group">
                        <label for="content">Address</label>
                        <textarea class="form-control" style="height: 150px;" name="address" rows="20" required><?php echo (set_value('address') ? set_value('address') : $result[0]->address); ?></textarea>
                    </div>


                    <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                <?php echo form_close(); ?>
            </div><!-- /.box -->







        </div>
    </div>

<?php $this->load->view('./admin/footer-link'); ?>
  
<?php $this->load->view('./admin/footer'); ?>