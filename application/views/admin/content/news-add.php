<?php $this->load->view('./admin/header'); ?>
<style>
.image-preview-input {
    position: relative;
    overflow: hidden;
    margin: 0px;    
    color: #333;
    background-color: #fff;
    border-color: #ccc;    
}
.image-preview-input input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
.image-preview-input-title {
    margin-left:2px;
}
</style>
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">News Add</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php
                if(validation_errors() || isset($error)){
                    echo "<div class='alert alert-danger'>";
                    echo validation_errors();
                    echo isset($error) ? $error : "";
                    echo "</div>";
                }
                ?>
                <?php
                //$array = array('enctype'=>'multipart/form-data');
                echo form_open_multipart('admin/content/news_add'); ?>
                <div class="box-body">


                    <div class="form-group">
                        <label for="header_image">Feature Image</label>
                        <div class="input-group image-preview">
                                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                <span class="input-group-btn">
                                    <!-- image-preview-clear button -->
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                        <span class="glyphicon glyphicon-remove"></span> Clear
                                    </button>
                                    <!-- image-preview-input -->
                                    <div class="btn btn-default image-preview-input">
                                        <span class="glyphicon glyphicon-folder-open"></span>
                                        <span class="image-preview-input-title">Browse</span>
                                        <input type="file" accept="image/png, image/jpeg, image/gif" name="feature_image"/> <!-- rename it -->
                                    </div>
                                </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="title">News title</label>
                        <input type="text" name="title" class="form-control" id="title" placeholder="Enter Title" value="<?php echo set_value('title'); ?>" autocomplete="off" >
                    </div>

                    <div class="form-group">
                        <label for="content">News</label>
                        <textarea class="form-control tinymce" style="height: 400px;" name="news" rows="20"><?php echo set_value('news'); ?></textarea>
                    </div>

                    <div class="form-group">
                        <label for="title">Publish Date</label>
                        <input type="date" name="publish_date" class="form-control" id="title" value="<?php echo set_value('publish_date'); ?>" autocomplete="off" style="width: 160px;" >
                    </div>

                    <div class="form-group">
                        <label for="ip_restriction">Status</label>
                        <div class="radio">
                            <label>
                                <input type="radio" name="status" id="status" class="status" value="1" required>
                                Active
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="status" id="status" class="status" value="0" required>
                                Inactive
                            </label>
                        </div>
                    </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                <?php echo form_close(); ?>
            </div><!-- /.box -->







        </div>
    </div>

<?php $this->load->view('./admin/footer-link'); ?>
    <script type="text/javascript" src="<?php print base_url();?>assets/js/tiny_mce/jquery.tinymce.js"></script>

    <script>

        // function string_to_slug(str) {
        //     str = str.replace(/^\s+|\s+$/g, ''); // trim
        //     str = str.toLowerCase();

        //     // remove accents, swap � for n, etc
        //     var from = "����������������������/_,:;";
        //     var to   = "aaaaeeeeiiiioooouuuunc------";
        //     for (var i=0, l=from.length ; i<l ; i++) {
        //         str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        //     }

        //     str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        //         .replace(/\s+/g, '-') // collapse whitespace and replace by -
        //         .replace(/-+/g, '-'); // collapse dashes

        //     return str;
        // }

        $(function(){         
            //For Tinymce
            $('textarea.tinymce').tinymce({
                // Location of TinyMCE script
                script_url : '<?php print base_url();?>assets/js/tiny_mce/tiny_mce.js',

                // General options
                theme : "advanced",
                plugins : "jbimages,autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",
                valid_elements : '+*[*]',
                // Theme options
                theme_advanced_buttons1 : "jbimages,|,save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
                theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
                theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
                theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
                theme_advanced_toolbar_location : "top",
                theme_advanced_toolbar_align : "left",
                theme_advanced_statusbar_location : "bottom",
                theme_advanced_resizing : true,

                // ===========================================
                // Set RELATIVE_URLS to FALSE (This is required for images to display properly)
                // ===========================================

                relative_urls : false
            });

            //For Slug
            $('#title').change(function(){
                //need to ajax 
                var url = $('#title').val().split("_");

                $.ajax({
                        url: '<?php echo base_url()."admin/ajax/" ?>show_submenu',
                        data: {'menu_id': url[0]},
                        type: "post",
                        dataType: 'text',
                        success: function (data) {
                            $('#submenu').html(data);
                        }
                });

                // var val = string_to_slug(url[1]);
                // $('#url').val(val);
            });

        });
    </script>
    <script type="text/javascript">
    $(document).on('click', '#close-preview', function(){ 
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
        function () {
           $('.image-preview').popover('show');
        }, 
         function () {
           $('.image-preview').popover('hide');
        }
    );    
});

$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    // Set the popover default content
    $('.image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
        content: "There's no image",
        placement:'bottom'
    });
    // Clear event
    $('.image-preview-clear').click(function(){
        $('.image-preview').attr("data-content","").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text("Browse"); 
    }); 
    // Create the preview image
    $(".image-preview-input input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-input-title").text("Change");
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);            
            img.attr('src', e.target.result);
            $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
        }        
        reader.readAsDataURL(file);
    });  
});
</script>

<?php $this->load->view('./admin/footer'); ?>