<?php $this->load->view('./admin/header'); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Table Quote View</h3>
                </div><!-- /.box-header -->

                <?php if($result == 0){ ?>
                    <div class="alert alert-danger">
                        <?php echo $this->session->flashdata('error_message'); ?>
                    </div>
                <?php }else{ ?>

                    <?php if($this->session->flashdata('success_message')): ?>
                        <div class="alert alert-success">
                            <?php echo $this->session->flashdata('success_message'); ?>
                        </div>
                    <?php endif; ?>

                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="custom">SL</th>
                                <th>Name</th>
                                <th>Designation</th>
                                <th>Quote</th>
                                <th>Status</th>
                                <th class="custom_last">Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            </tfoot>
                            <tbody>
                            <?php $sl = 1;
                            $satus = array('0'=>'<span style="color:red">Inactive</span>','1'=>'<span style="color:green">Active</span>');

                            foreach($result as $value){ ?>
                                <tr>
                                    <td><?php echo $sl; ?></td>
                                    <td><?php echo $value->name; ?></td>
                                    <td><?php echo $value->designation; ?></td>
                                    <td><?php echo $value->quote; ?></td>
                                    
                                    <td><?php echo $satus[$value->status] ?></td>
                                    <td>
                                        <a href="quote_edit/<?php echo $value->id ?>" title="Edit"><i class="fa fa-fw fa-edit"></i></a> /
                                        <a href="quote_delete/<?php echo $value->id ?>" onclick="return confirm('Are you sure you wish to delete this ?')" title="Delete"><i class="fa fa-fw fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php $sl++; } ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                <?php } ?>
            </div><!-- /.box -->
        </div>
    </div>

<?php $this->load->view('./admin/footer-link') ?>

    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#example1").DataTable();
        });
    </script>


<?php $this->load->view('./admin/footer'); ?>