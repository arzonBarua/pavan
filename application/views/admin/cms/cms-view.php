<?php $this->load->view('./admin/header'); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Table CMS View</h3>
                </div><!-- /.box-header -->

                <?php if($result == 0){ ?>
                    <div class="alert alert-danger">
                        <?php echo $this->session->flashdata('error_message'); ?>
                    </div>
                <?php }else{ ?>

                    <?php if($this->session->flashdata('success_message')): ?>
                        <div class="alert alert-success">
                            <?php echo $this->session->flashdata('success_message'); ?>
                        </div>
                    <?php endif; ?>

                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="custom">SL</th>
                                <th>Menu</th>
                                <th>Submenu</th>
                                <th>Status</th>
                                <th class="custom_last">Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <td></td>
                                <td></td>
                                <td colspan="3"></td>
                            </tr>
                            </tfoot>
                            <tbody>
                            <?php $sl = 1;

                            $menuPostion = array('1'=>'Top Menu','2'=>'Footer','3'=>'Both menu');
                            $satus = array('0'=>'<span style="color:red">Inactive</span>','1'=>'<span style="color:green">Active</span>');
                            foreach($result as $value){ ?>
                                <tr>
                                    <td><?php echo $sl; ?></td>
                                    <td><?php echo $value->menu_name; ?></td>
                                    <td><?php echo $value->submenu_name; ?></td>
                                    
                                    <td><?php echo $satus[$value->status] ?></td>
                                    <td>
                                        <div class="modal fade" id="<?php echo $value->id ?>" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">View CMS</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table class="table table-hover table-nomargin table-bordered">
                                                            <tbody>
                                                            <tr>
                                                                <td>Menu</td>
                                                                <td align="center">
                                                                    <?php echo $value->menu_name; ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Submenu</td>
                                                                <td align="center">
                                                                    <?php echo $value->submenu_name; ?>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>Header Image</td>
                                                                <td align="center">
                                                                    <img src="<?php echo base_url()."assets/upload/header_image/".$value->header_image ?>" style="width:250px">
                                                                </td>
                                                            </tr> 

                                                            <tr>
                                                                <td>URL</td>
                                                                <td align="center">
                                                                    <?php echo $value->url; ?>
                                                                </td>
                                                            </tr>     

                                                            <tr>
                                                                <td>Custom URL</td>
                                                                <td align="center">
                                                                    <?php echo $value->customurl; ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">Content</td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" align="center">
                                                                    <?php echo $value->content; ?>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td colspan="2">Style Sheet</td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" align="center">
                                                                    <?php echo $value->styletype; ?>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td colspan="2">Javascript</td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" align="center">
                                                                    <?php echo $value->scripttype; ?>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>Meta Keywords</td>
                                                                <td align="center">
                                                                    <?php echo $value->metakeyword; ?>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>Meta Description</td>
                                                                <td align="center">
                                                                    <?php echo $value->metadescp; ?>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>Status</td>
                                                                <td align="center">
                                                                    <?php echo $satus[$value->status]; ?>
                                                                </td>
                                                            </tr>



                                                            </tbody>
                                                        </table>


                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="javascript:void(0)" title="view" data-toggle="modal" data-target="#<?php echo $value->id ?>" ><i class="fa fa-fw fa-eye"></i></a> /
                                        <a href="cms_edit/<?php echo $value->id ?>" title="Edit"><i class="fa fa-fw fa-edit"></i></a> /
                                        <a href="cms_delete/<?php echo $value->id ?>" onclick="return confirm('Are you sure you wish to delete this ?')" title="Delete"><i class="fa fa-fw fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php $sl++; } ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                <?php } ?>
            </div><!-- /.box -->
        </div>
    </div>

<?php $this->load->view('./admin/footer-link') ?>

    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#example1").DataTable({
                initComplete: function () {
                    this.api().columns([2]).every( function () {
                        var column = this;
                        var select = $('<select class="form-control"><option value=""> - Select All - </option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search( val ? '^'+val+'$' : '', true, false )
                                    .draw();
                            } );

                        column.data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );
                    } );
                }
            });
        });
    </script>


<?php $this->load->view('./admin/footer'); ?>