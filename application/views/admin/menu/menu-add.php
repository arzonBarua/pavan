<?php $this->load->view('./admin/header'); ?>
    <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Add Menu</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php
                //$array = array('enctype'=>'multipart/form-data');
                echo form_open_multipart('admin/menu/menu_add'); ?>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name">Menu Name</label>
                            <input type="text" name="name" class="form-control" id="Name" placeholder="Enter Menu Name" autocomplete="off" required>
                        </div>
                        <div class="form-group">
                            <label for="order_id">Order ID</label>
                            <input type="text" name="order_id" class="form-control" id="order_id" placeholder="Enter Order ID" autocomplete="off" required>
                        </div>
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                <?php echo form_close(); ?>
            </div><!-- /.box -->




        </div>
    </div>

<?php $this->load->view('./admin/footer-link'); ?>

<?php $this->load->view('./admin/footer'); ?>