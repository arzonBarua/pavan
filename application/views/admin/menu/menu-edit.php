<?php $this->load->view('./admin/header'); ?>

    <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Edit Menu</h3>
                </div><!-- /.box-header -->

                <?php if($result == 0){ ?>
                    <div class="alert alert-danger">
                        <?php echo $this->session->flashdata('error_message'); ?>
                    </div>
                <?php }else{ ?>
                    <?php
                //$array = array('enctype'=>'multipart/form-data');
                    echo form_open_multipart('admin/menu/menu_edit/'.$result[0]->id); ?>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Menu Name</label>
                                <input type="text" name="name" class="form-control" id="Name" placeholder="Enter Menu Name" autocomplete="off" value="<?php echo $result[0]->name ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="order_id">Order ID</label>
                                <input type="text" name="order_id" class="form-control" id="order_id" placeholder="Enter Order ID" autocomplete="off" value="<?php echo $result[0]->order_id ?>" required>
                            </div>    
                        </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                    <?php echo form_close(); ?>
                </div><!-- /.box -->
            <?php } ?>
        </div>
    </div>

<?php $this->load->view('./admin/footer-link'); ?>

    <script>
        $(function(){
            $('.ip_restriction').change(function(){
                if($(this).is(":checked")){
                    var val = $(this).val();
                    if(val==1){
                        $('#ip_address').show();
                        $('#ip_address_val').attr('required','required');
                    }else{
                        $('#ip_address').hide();
                        $('#ip_address_val').removeAttr('required');
                        $('#ip_address_val').val("");
                    }
                }
            });
        });
    </script>

<?php $this->load->view('./admin/footer'); ?>