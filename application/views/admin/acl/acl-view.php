<?php $this->load->view('./admin/header'); ?>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Table ACL</h3>
            </div><!-- /.box-header -->



            <?php if($result == 0){ ?>
                <div class="alert alert-danger">
                    <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php }else{ ?>

                <?php if($this->session->flashdata('success_message')): ?>
                        <div class="alert alert-success">
                            <?php echo $this->session->flashdata('success_message'); ?>
                        </div>
                <?php endif; ?>

                <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th class="custom">SL</th>
                        <th>Name</th>
                        <th class="custom_last">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $sl = 1;
                    foreach($result as $value){ ?>
                             <tr>
                                <td><?php echo $sl; ?></td>
                                <td><?php echo $value->full_name; ?></td>
                                <td>
                                    <div class="modal fade" id="<?php echo $value->id ?>" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">View ACL</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <?php
                                                    $array = json_decode($value->access);
                                                    foreach($array as $key => $val ){ ?>
                                                        <div>
                                                            <label class="control-label"><?php echo $name[$key]; ?></label>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="checkbox">
                                                                <ul>
                                                                <?php foreach($val as $list){ ?>
                                                                    <li><?php echo $name[$list]; ?></li>
                                                                <?php } ?>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="javascript:void(0)" title="view" data-toggle="modal" data-target="#<?php echo $value->id ?>" ><i class="fa fa-fw fa-eye"></i></a> /
                                    <a href="acl_edit/<?php echo $value->id ?>" title="Edit"><i class="fa fa-fw fa-edit"></i></a> /
                                    <a href="acl_delete/<?php echo $value->id ?>" onclick="return confirm('Are you sure you wish to delete this ?')" title="Delete"><i class="fa fa-fw fa-trash-o"></i></a>
                                </td>
                            </tr>
                    <?php $sl++; } ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
            <?php } ?>
        </div><!-- /.box -->
    </div>
</div>

<?php $this->load->view('./admin/footer-link') ?>

<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        $("#example1").dataTable();
    });
</script>


<?php $this->load->view('./admin/footer'); ?>
