<?php $this->load->view('./admin/header'); ?>

        <div class="row">
            <div class="col-md-6">
                <!-- general form elements disabled -->
                <div class="box box-warning">
                    <div class="box-header">
                        <h3 class="box-title">ACL</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <?php if($this->session->flashdata('Insertion_failed')): ?>
                            <div class="alert alert-danger">
                                <?php echo $this->session->flashdata('Insertion_failed'); ?>
                            </div>
                        <?php endif; ?>
                        <?php echo form_open('admin/acl/acl_add'); ?>
                                <div class="form-group">
                                    <label>Select User</label>
                                    <select class="form-control" name="login_id">
                                        <option>--Select--</option>

                                        <?php
                                        if ($get_user_all) {
                                            foreach ($get_user_all as $key => $val) { ?>
                                                <option value="<?php echo $key ?>"><?php echo $val; ?></option>
                                            <?php }
                                        }
                                        ?>

                                    </select>
                                </div>

                                <?php foreach($result as $key => $val ){ ?>
                                    <div class="form-group">
                                        <label class="control-label"><i class="fa fa-fw fa-unlock-alt"></i> <?php echo $name[$key]; ?></label>
                                    </div>
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <?php foreach($val as $value){ ?>
                                                    <label style="margin-right:10px">
                                                        <input type="checkbox" name="access[<?php echo $key ?>][]" value="<?php echo $value; ?>" class="<?php echo current(explode(' ',$name[$key])); ?>" />
                                                        <?php echo $name[$value]; ?>
                                                    </label>
                                            <?php } ?>
                                                    <label>
                                                        <input type="checkbox" class="select-all" id="<?php echo current(explode(' ',$name[$key])); ?>">Full
                                                    </label>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                        <?php echo form_close(); ?>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!--/.col (right) -->
        </div>

<?php $this->load->view('./admin/footer-link'); ?>

<script>
    $(function(){
        $('.select-all').click(function(){
            var val = $(this).attr('id');
            if($(this).is(":checked")){
                $('.'+val).prop('checked',true);
            }else{
                $('.'+val).prop('checked',false);
            }
        });
    });
</script>

<?php $this->load->view('./admin/footer'); ?>

