<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Pavan Zentrum für Kundalini Yoga - Marina Loewen</title>

    <!-- responsive meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- master stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url()."assets/site/css/style.css" ?>">
    <!-- responsive stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url()."assets/site/css/responsive.css" ?> ">


</head>
<body>
    <!-- Preloader -->
    <div class="preloader"></div>
<!--Start header area-->  
<section class="header_area ">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <div class="col-md-2 col-sm-4 logo">
               <a href="index.html"><img src="<?php echo base_url()."assets/site/" ?>img/header/logo.png" alt="logo"></a>
            </div>
            <div class="col-md-10 col-sm-8 brandName text-left">
              <h2>Pavan-Zentrum <small>für Kundalini Yoga</small></h2>
            </div>
          </div>
            <!-- <div class="col-md-7 col-md-offset-1 col-sm-7 col-sm-offset-1">
                <div class="coll_us_location">
                    <div class="coll_us our_location">
                        <div class="coll_img_holder"><i class="fa fa-phone"></i></div>
                        <h4>Jetzt anrufen</h4>
                        <p>+49 177 387 2012</p>
                    </div>
                    <div class="donate">
                        <div class="coll_img_holder"><i class="fa fa-map-marker"></i></div>
                        <h4>Unsere Adresse</h4>
                        <p>Nimweger Str. 174, 47533 Kleve</p>
                    </div>
                    <div class="donate"><h2><a href="#">Donate Now</a></h2></div>
                </div>
            </div> -->
        </div>
    </div>
</section>
<!--End header area-->
<!--Start header area-->
<section class="header_area">
    <div class="container header">  
        <nav class="main-menu text-center">
            <!--<div class="header-serch-donate pull-right">
                <div class="social_icon">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook " aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                <div class="top-search-box">
                    <button><i class="fa fa-search"></i></button>
                    <ul class="search-box">
                        <li>
                            <form action="#">
                                <input type="text" placeholder="Search for something...">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
                        </li>
                    </ul>   
                </div>
            </div>-->
            <div class="navbar-header">     
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse clearfix">
                <ul class="navigation aligncenter">
                    <li><a href="index.html">Home</a></li>
                    <li class="dropdown"><a href="#">Kundalini Yoga</a>
                        <ul>
                            <li><a href="lessons.html">Menu 1</a></li>
                            <li><a href="about.html">Menu 2</a></li>
                        </ul>
                    </li>
                    <li><a href="youga.html">Mein Angebot</a></li>
                    <li><a href="about.html">Über Mich</a></li>                    
                    <li><a href="contact.html">Kontakt</a></li>
                </ul>
            </div>
        </nav>   
    </div>
</section>
<!--End header area-->