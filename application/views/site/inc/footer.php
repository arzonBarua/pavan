 <section class="footer_area">
    <div class="container">
        <div class="row">
				<div  class="col-md-4 col-sm-4">
					<div class="footer_contact_recent contact_details">
						<h2>Kontakt</h2>
                        <div class="border_contact_recent"></div>
						<p> <i class="fa fa-phone"></i> +49 177 387 2012<br>
                            <i class="fa fa-envelope"></i> yoga@pavan-zentrum.de<br>
                            <i class="fa fa-map-marker"></i> Nimweger Str. 174, 47533 Kleve</p>
					</div>
				</div>
				<div  class="col-md-4 col-sm-4 ">
					<div class="footer_contact_recent contact_details  border_contact_details">
						<h2>Über das Pavan-Zentrum</h2>
                        <div class="border_contact_recent"></div>
						<p>The majority have suffered alteration in some form, 
						by injected humour, or randomised words which
						don’t look even slightly believable.</p>
						<a href="#">Mehr erfahren...</a>
					</div>
				</div>
				<div  class="col-md-4 col-sm-4">
					<div class="footer_contact_recent services_contact">
				    <div class="text-center">
                    	<h2>Wichtige Links</h2>
                    	<div class="border_contact_recent" ></div>
                    </div>
                    <div class="footer_contact_services">
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li><a href="lessons.html">Yoga Stunden</a></li>
                            <li><a href="contact.html">Kontakt</a></li>
                        </ul>
                    </div>
				</div>
                    
				</div>
			</div>
    </div>
</section>
  <!--End footer area-->  
 <!--Start footer bottom area-->
 <section class="footer_bottom_area">
    <div class="container"> 
        <div class="row">
            <div  class="col-md-6 col-sm-6">
                <div class="footer_logo">
                    <p>Copyright &copy; 2017. Alle Rechte vorbehalten.</p>
                </div>
            </div>
            <div  class="col-md-6 col-sm-6">
                <div class="footer_bottom_right">
                    <a href="#">Impressum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                </div>
             </div>
        </div>
    </div>
</section> 
    
 <!--End footer bottom area-->      

	<script src="<?php echo base_url()."assets/site/" ?>plugin/jquery/jquery-1.12.5.js"></script>
	<!-- bootstrap js -->
	<script src="<?php echo base_url()."assets/site/" ?>plugin/bootstrap/js/bootstrap.min.js"></script>
	<!-- bx slider -->
	<script src="<?php echo base_url()."assets/site/" ?>plugin/jquery.bxslider.min.js"></script>
	<!-- count to -->
	<script src="<?php echo base_url()."assets/site/" ?>plugin/jquery.countTo.js"></script>
	<!-- owl carousel js -->
	<script src="<?php echo base_url()."assets/site/" ?>plugin/owl.carousel-2/owl.carousel.min.js"></script>
	<!-- validate -->
	<script src="<?php echo base_url()."assets/site/" ?>plugin/validate.js"></script>
	<!-- appear -->
	<script src="<?php echo base_url()."assets/site/" ?>js/jquery.appear.js"></script>
	<!--concat -->
	<script src="<?php echo base_url()."assets/site/" ?>js/jquery.mCustomScrollbar.concat.min.js"></script>
	<!-- mixit up -->
	<script src="<?php echo base_url()."assets/site/" ?>plugin/jquery.mixitup.min.js"></script>
	<!-- fancybox -->
	<script src="<?php echo base_url()."assets/site/" ?>plugin/jquery.fancybo<?php echo base_url()."assets/site/" ?>x.pack.js"></script>
	<!-- easing -->
	<script src="<?php echo base_url()."assets/site/" ?>plugin/jquery.easing.min.js"></script>
	<!-- video responsive script -->
    <script src="<?php echo base_url()."assets/site/" ?>plugin/jquery.fitvids.js"></script>
    <!-- menuzord script -->
   <script src="<?php echo base_url()."assets/site/" ?>plugin/js/menuzord.js"></script>
	<script src="<?php echo base_url()."assets/site/" ?>js/jquery.countdown.js"></script>
	<script src="<?php echo base_url()."assets/site/" ?>js/jquery.scrollUp.js"></script> 

    <!-- revolution scripts -->
	<script src="<?php echo base_url()."assets/site/" ?>revolution/js/jquery.themepunch.tools.min.js"></script>
	<script src="<?php echo base_url()."assets/site/" ?>revolution/js/jquery.themepunch.revolution.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url()."assets/site/" ?>revolution/js/extensions/revolution.extension.actions.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url()."assets/site/" ?>revolution/js/extensions/revolution.extension.carousel.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url()."assets/site/" ?>revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url()."assets/site/" ?>revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url()."assets/site/" ?>revolution/js/extensions/revolution.extension.migration.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url()."assets/site/" ?>revolution/js/extensions/revolution.extension.navigation.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url()."assets/site/" ?>revolution/js/extensions/revolution.extension.parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url()."assets/site/" ?>revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url()."assets/site/" ?>revolution/js/extensions/revolution.extension.video.min.js"></script>
    
    


	<!-- thm custom script -->
	<script src="<?php echo base_url()."assets/site/" ?>js/custom.js"></script>
    


</body>
</html>