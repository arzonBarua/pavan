<!DOCTYPE html>
<html lang="de">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">

<!--    <meta http-equiv="X-UA-Compatible" content="IE=edge">-->
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <?php if(!empty($metakeyword) && !empty($metadescp) ){ ?>
        <meta name="keywords" content="<?php echo $metakeyword ?>">
        <meta name="description" content="<?php echo $metadescp ?>">
    <?php }else { ?>
        <meta name="keywords" content="">
        <meta name="description" content="">
    <?php } ?>

    <meta name="author" content="">





    <title>wechselstrom</title>

    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/fav.png" />


    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">


    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        html,
        body {
            height: 100%;
            /* The html and body elements cannot have any padding or margin. */
        }

        /* Wrapper for page content to push down footer */
        #wrap {
            min-height: 100%;
            height: auto !important;
            height: 100%;
            /* Negative indent footer by it's height */
            margin: 0 auto -100px;
        }

        /* Set the fixed height of the footer here */
        #push,
        #footer {
            height: 100px;
        }
        #footer {
            background-color: #f5f5f5;
        }

        /* Lastly, apply responsive CSS fixes as necessary */
        @media (max-width: 767px) {
            #footer {
                margin-left: -20px;
                margin-right: -20px;
                padding-left: 20px;
                padding-right: 20px;
            }
        }
    </style>

</head>


<body>
<div id="wrap">
    <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!--<a class="navbar-brand" href="index-2.html">SOLID.</a>-->
                    <ul class="nav navbar-nav nav-edit">
                        <li class="<?php echo ($page=='home') ? 'active' : '' ?>"><a href="<?php echo base_url(); ?>">HOME</a></li>
                        <li class="<?php echo ($page=='contact') ? 'active' : '' ?>"><a href="<?php echo base_url().'contact'; ?>">ANGEBOT ANFORDERN</a></li>
                        <!-- <li><a href="about.html">&Uuml;BER UNS</a></li>
                        <li><a href="contact.html">KONTAKT</a></li> -->
                        <?php $this->Common_operation->menu('1',$page); ?>
                        <!--<li class="<?php echo ($page=='agb') ? 'active' : '' ?>"><a href="<?php echo base_url().'agb'; ?>">ALLGEMEINE GESCH&Auml;FTSBEDINGUNGEN</a> -->

<!--                        <li class="dropdown">-->
<!--                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">PAGES <b class="caret"></b></a>-->
<!--                            <ul class="dropdown-menu">-->
<!--                                <li><a href="blog.html">BLOG</a></li>-->
<!--                                <li><a href="single-post.html">SINGLE POST</a></li>-->
<!--                                <li><a href="portfolio.html">PORTFOLIO</a></li>-->
<!--                                <li><a href="single-project.html">SINGLE PROJECT</a></li>-->
<!--                            </ul>-->
<!--                        </li>-->
                    </ul>
            </div>
            <div class="navbar-right">
              <!--   <img src="assets/images/logo.png" class="img-res"> -->
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav nav-edit-collapse">
                        <li class="<?php echo ($page=='home') ? 'active' : '' ?>"><a href="<?php echo base_url(); ?>">HOME</a></li>
                        <li class="<?php echo ($page=='contact') ? 'active' : '' ?>"><a href="<?php echo base_url().'contact'; ?>">ANGEBOT ANFORDERN</a></li>
                        <!-- <li><a href="about.html">&Uuml;BER UNS</a></li>
                        <li><a href="contact.html">KONTAKT</a></li> -->
                        <?php $this->Common_operation->menu('1',$page); ?>
                        
                    </ul>
                </div>
            </div><!--/.nav-collapse -->
        </div>
    </div>