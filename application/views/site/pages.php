<?php $this->load->view('site/inc/head'); ?>
<style>
    <?php echo $result[0]->styletype ?>
</style>
<!--body content-->
<div id="blue">
    <div class="container">
        <div class="row">
            <h3><?php echo $result[0]->title; ?>.</h3>
             <a href="<?php echo base_url() ?>"><img src="assets/images/logo.png" class="img-res pull-right"></a>
        </div><!-- /row -->
    </div> <!-- /container -->
</div>


<div class="container mtb">
    <?php echo $result[0]->content; ?>
</div>

<div id="push"></div>

<?php  $this->load->view('site/inc/footer-link'); ?>

<!--custom script-->
<script>
    <?php echo $result[0]->scripttype; ?>
</script>

<?php  $this->load->view('site/inc/footer'); ?>
