﻿<!DOCTYPE html>
<html lang="de">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<head>
<style>
	table {
	    width:100%;
	}
	table, th, td {
	    border: 1px solid black;
	    border-collapse: collapse;
	}
	th, td {
	    padding: 5px;
	    text-align: left;
	}
	table#t01 tr:nth-child(even) {
	    background-color: #eee;
	}
	table#t01 tr:nth-child(odd) {
	   background-color:#fff;
	}
	table#t01 th {
	    background-color: rgb(31,78,121);
	    color: white;
	}
</style>
</head>
<body>
<table id="t01">
	  <tr>
	    <th colspan="2">Kundeneingabe:</th>
	  </tr>


	  	<?php if($result['radio1']=='1'){ ?>
			<tr>
				<td colspan="2">Ich beauftrage WECHSELSTROM mit der Vertragsoptimierung im Bereich:</td>
			</tr>
		<?php }elseif($result['radio1']=='2'){ ?>	
		<!--There will be another base on logic-->
			<tr>
				<td colspan="2">Ich fordere ein unverbindliches Angebot an im Bereich:</td>
			</tr>
		<?php } ?>	

		  	<?php if($result['radio2']=='1'){ ?>
			<tr>
				<td colspan="2">Strom + Gas</td>
			</tr>
		<?php }elseif($result['radio2']=='2'){ ?>	
		
			<tr>
				<td colspan="2">Strom</td>
			</tr>
		<?php }elseif($result['radio2']=='3'){ ?>	
		
			<tr>
				<td colspan="2">Gas</td>
			</tr>
		<?php } ?>
	
<?php if($result['radio3']=='1'){ ?>
			<tr>
				<td colspan="2">Ich sende WECHSELSTROM die letzte Jahresrechnung zu</td>
			</tr>
		<?php }elseif($result['radio3']=='2'){ ?>	
		<!--There will be another base on logic-->
			<tr>
				<td colspan="2">Ich trage die Daten manuell ein:</td>
			</tr>
		<?php } ?>	
		
		


		<!--Step 2-->
			  <tr>
				<td>Anrede</td>
				<td><?php echo $result['anrede']; ?></td>
			  </tr>
			  <tr>
				<td>Vorname</td>
				<td><?php echo $result['vorname']; ?></td>
			  </tr>
			  <tr>
				<td>Nachname</td>
				<td><?php echo $result['nachname']; ?></td>
			  </tr>

			  <tr>
				<td>Straße</td>
				<td><?php echo $result['str']; ?></td>
			  </tr>
			  <tr>
				<td>Hausnummer</td>
				<td><?php echo $result['hausnummer']; ?></td>
			  </tr>
			  <tr>
				<td>PLZ</td>
				<td><?php echo $result['plz']; ?></td>
			  </tr>
			  <tr>
				<td>Ort</td>
				<td><?php echo $result['stadt']; ?></td>
			  </tr>
			  <tr>
				<td>E-Mail</td>
				<td><?php echo $result['mail']; ?></td>
			  </tr>
			  <tr>
				<td>Geburtsdatum</td>
				<td><?php echo $result['geburtsdatum']; ?></td>
			  </tr>
			  <tr>
				<td>Telefonnummer</td>
				<td><?php echo $result['telefonnummer']; ?></td>
			  </tr>
		<!--End Step 2-->

		<tr>
			<td colspan="2" style="height: 20px;"></td>
		</tr>

		<!--step 3-->
				<?php if($result['radio3']=='2'){ ?>
						<?php if(($result['radio2']=='1') || ($result['radio2']=='2')) { ?>
							<tr>
								<td>Zählernummer Strom</td>
								<td><?php echo $result['zahlernummer_strom']; ?></td>
							</tr>
							<tr>
								<td>Jahresverbrauch Strom (kWh)</td>
								<td><?php echo $result['jahresverbrauch_strom']; ?></td>
							</tr>
							<tr>
								<td>Alternativ: Anzahl Personen im Haushalt</td>
								<td><?php echo $result['anzahl_personen']; ?></td>
							</tr>

							<tr>
								<td>Derzeitiger Stromlieferant</td>
								<td><?php echo $result['alternativ_strom']; ?></td>
							</tr>
							<tr>
								<td>Derzeitiger Stromtarif</td>
								<td><?php echo $result['aktueller_strom']; ?></td>
							</tr>
						<?php } ?>	
						<?php if(($result['radio2']=='1') || ($result['radio2']=='3')) { ?>
							<tr>
								<td>Zählernummer Gas</td>
								<td><?php echo $result['zahlernummer_gas']; ?></td>
							</tr>
							<tr>
								<td>Jahresverbrauch Gas (kWh)</td>
								<td><?php echo $result['jahresverbrauch_gas']; ?></td>
							</tr>
							<tr>
								<td>Wohnfläche (ca.)</td>
								<td><?php echo $result['wohnflache_gas']; ?></td>
							</tr>
							<tr>
								<td>Derzeitiger Gaslieferant</td>
								<td><?php echo $result['gaslieferant_gas']; ?></td>
							</tr>
							<tr>
								<td>Derzeitiger Gastarif</td>
								<td><?php echo $result['gastarif_gas']; ?></td>
							</tr>
							<?php } ?>
					<?php } ?>
							<tr>
								<td>Anmerkungen:</td>
								<td><?php echo $result['kommentarfeld']; ?></td>
							</tr>
		<!--End step 3-->
		<tr>
			<td colspan="2" style="height: 20px;"></td>
		</tr>

		<!--Step 4-->
		<tr>
			<td colspan="2" style="height: 20px;">Ich möchte die Auftragsbestätigung bzw. das Angebot erhalten</td>
		</tr>

		<?php if($result['radio4']=='1'){ ?>
			<tr>
				<td colspan="2">per Mail</td>
			</tr>
		<?php }elseif($result['radio4']=='2'){ ?>	
		
			<tr>
				<td colspan="2">per Post</td>
			</tr>
		<?php } ?>



		<?php if(!empty($result['radio5']) && ($result['radio5']=='1')){ ?>
			<tr>
				<td colspan="2" style="height: 20px;">Ich möchte bequem per Lastschrift zahlen</td>
			</tr>
		<?php } ?>
		<?php if(!empty($result['radio5']) && ($result['radio5']=='2')){ ?>
			<tr>
				<td colspan="2" style="height: 20px;">Ich bezahle auf Rechnung per Überweisung</td>
			</tr>
		<?php } ?>


		<?php if(isset($result['checkbox1'])){ ?>
			<tr>
				<td colspan="2" style="height: 20px;"><?php echo $result['checkbox1']; ?></td>
			</tr>
		<?php } ?>	
		<?php if(isset($result['checkbox2'])){ ?>
			<tr>
				<td colspan="2" style="height: 20px;"><?php echo $result['checkbox2']; ?></td>
			</tr>
		<?php } ?>	
		<?php if(isset($result['checkbox3'])){ ?>
			<tr>
				<td colspan="2" style="height: 20px;"><?php echo $result['checkbox3']; ?></td>
			</tr>
		<?php } ?>
		<?php if(($result['radio1']=='2') || ($result['radio5']=='1')) { ?>
			<tr>
				<td>Kontoinhaber</td>
				<td><?php echo $result['kontoinhaber']; ?></td>
			</tr>
		<?php } ?>
		<?php if(($result['radio1']=='2') || ($result['radio5']=='1')) { ?>
		<tr>
			<td>IBAN</td>
			<td><?php echo $result['iban']; ?></td>
		</tr>
		<?php } ?>
		<!--End Step 4-->

</table>
</body>
</html>
