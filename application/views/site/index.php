﻿<?php $this->load->view("site/inc/head"); ?>
<!--Start rev slider wrapper-->     
<section class="rev_slider_wrapper">
    <div id="slider1" class="rev_slider" data-version="5.0">
        <ul>
            <li data-transition="fade">
                <img src="<?php echo base_url()."assets/site/" ?>img/slides/bg2.jpg"  alt="" width="1920" height="1020" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >
                
                <div class="tp-caption tp-resizeme go_green"
                    data-x="left" data-hoffset="0" 
                    data-y="top" data-voffset="170" 
                    data-transform_idle="o:1;"         
                    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;" 
                    data-splitin="none" 
                    data-splitout="none"  
                    data-start="500">
                    <h2>Kundalini Yoga</h2>
                </div>
                <div class="tp-caption tp-resizeme go_green"
                    data-x="left" data-hoffset="0" 
                    data-y="top" data-voffset="238"  
                    data-transform_idle="o:1;"                         
                    data-transform_in="x:[175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                    data-mask_in="x:[-100%];y:0;s:inherit;e:inherit;" 
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="1500">
                    <h4>in Kleve mit Marina Loewen</h4>
                    <div class="border"></div>
                </div>
                <div class="tp-caption tp-resizeme go_green" 
                    data-x="left" data-hoffset="0" 
                    data-y="top" data-voffset="370"  
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="2000">
                    <p>Besuchen sie mich im Pavan-Zentrum<br>in entspannter Lage zwischen Kleve und Kranenburg.</p>
                </div>
                <!--<div class="tp-caption tp-resizeme go_green" 
                    data-x="left" data-hoffset="0" 
                    data-y="top" data-voffset="440"  
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="3000">
                    <div class="join"><a href="#">Join Now </a></div>
                </div>
                <div class="tp-caption tp-resizeme go_green" 
                    data-x="left" data-hoffset="165" 
                    data-y="top" data-voffset="440"  
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="3000">
                    <div class="donate"><a href="#">Donate Now</a></div>
                </div>-->
            </li>
            <li data-transition="parallaxvertical">
                <img src="<?php echo base_url()."assets/site/" ?>img/slides/bg_1.jpg"  alt="" width="1920" height="1020" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >
                
                <div class="tp-caption tp-resizeme go_green"
                    data-x="left" data-hoffset="0" 
                    data-y="top" data-voffset="170" 
                    data-transform_idle="o:1;"         
                    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;" 
                    data-splitin="none" 
                    data-splitout="none"  
                    data-start="500">
                    <h2>Kundalini Yoga</h2>
                </div>
                <div class="tp-caption tp-resizeme go_green"
                    data-x="left" data-hoffset="0" 
                    data-y="top" data-voffset="238"  
                    data-transform_idle="o:1;"                         
                    data-transform_in="x:[175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                    data-mask_in="x:[-100%];y:0;s:inherit;e:inherit;" 
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="1500">
                    <h4>in Kleve mit Marina Loewen</h4>
                    <div class="border"></div>
                </div>
                <div class="tp-caption tp-resizeme go_green" 
                    data-x="left" data-hoffset="0" 
                    data-y="top" data-voffset="370"  
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="2000">
                    <p>Besuchen sie mich im Pavan-Zentrum<br>in entspannter Lage zwischen Kleve und Kranenburg.</p>
                </div>
                <!--<div class="tp-caption tp-resizeme go_green" 
                    data-x="left" data-hoffset="0" 
                    data-y="top" data-voffset="440"  
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="3000">
                    <div class="join"><a href="#">Join Now </a></div>
                </div>
                <div class="tp-caption tp-resizeme go_green" 
                    data-x="left" data-hoffset="165" 
                    data-y="top" data-voffset="440"  
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="3000">
                    <div class="donate"><a href="#">Donate Now</a></div>
                </div>-->
            </li>
            <li data-transition="slidingoverlayleft">
                <img src="<?php echo base_url()."assets/site/" ?>img/slides/bg3.jpg"  alt="" width="1920" height="1020" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >
                
                <div class="tp-caption tp-resizeme go_green"
                    data-x="left" data-hoffset="0" 
                    data-y="top" data-voffset="170" 
                    data-transform_idle="o:1;"         
                    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;" 
                    data-splitin="none" 
                    data-splitout="none"  
                    data-start="500">
                    <h2>Kundalini Yoga</h2>
                </div>
                <div class="tp-caption tp-resizeme go_green"
                    data-x="left" data-hoffset="0" 
                    data-y="top" data-voffset="238"  
                    data-transform_idle="o:1;"                         
                    data-transform_in="x:[175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                    data-mask_in="x:[-100%];y:0;s:inherit;e:inherit;" 
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="1500">
                    <h4>in Kleve mit Marina Loewen</h4>
                    <div class="border"></div>
                </div>
                <div class="tp-caption tp-resizeme go_green" 
                    data-x="left" data-hoffset="0" 
                    data-y="top" data-voffset="370"  
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="2000">
                    <p>Besuchen sie mich im Pavan-Zentrum<br>in entspannter Lage zwischen Kleve und Kranenburg.</p>
                </div>
                <!--<div class="tp-caption tp-resizeme go_green" 
                    data-x="left" data-hoffset="0" 
                    data-y="top" data-voffset="440"  
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="3000">
                    <div class="join"><a href="#">Join Now </a></div>
                </div>
                <div class="tp-caption tp-resizeme go_green" 
                    data-x="left" data-hoffset="165" 
                    data-y="top" data-voffset="440"  
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="3000">
                    <div class="donate"><a href="#">Donate Now</a></div>
                </div>-->
            </li>
        </ul>
    </div>
</section>
<!--End rev slider wrapper-->
    
<!--Start join_events area--> 
<section class="join_makes_area clearfix">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12"> 
                
                <div class="how_to_pollution polution_margin">
                    <div class="pollution_img_holder">
                        <img src="<?php echo base_url()."assets/site/" ?>img/1.jpg" alt="">
                    </div>
                    <div class="pollution_box">
                        <div class="pollution_text">
                            <a href="blog-details.html"><h2>Herzlich willkommen!</h2></a>
                            <p>If you are going to use a passage of  Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of texttend to repeat the predefined chunks. If you are going to use a passage of  Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of texttend to repeat the predefined chunks.<br><br> If you are going to use a passage of  Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of texttend to repeat the predefined chunks.<br> <a href="#">Mehr erfahren...</a></p>
                        </div>
                        <div class="visual_designer">
                            <div class="visual_designer_text">
                                <img src="<?php echo base_url()."assets/site/" ?>img/blog/11.png" alt="">
                                <h2>Marina Loewen</h2>
                                <p>Yoga Lehrerin</p>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="col-lg-4 col-md-12">            	
               <div class="free-consulting"> 
                <div class="inner-content">
                <div class="content">
                   <div class="join_makes ">
                   	   <div class="whitebox">
                            <h2>Kontakt</h2>						
                            <p stlye=""><i class="fa fa-phone"></i> +49 177 387 2012<br>
                                <i class="fa fa-envelope"></i> yoga@pavan-zentrum.de<br>
                                <i class="fa fa-map-marker"></i> Nimweger Str. 174, 47533 Kleve</p>
                        </div>
                        <div class="whitebox">
                            <h2>Aktuelles</h2>
                            <!--news  start-->
                            <div class="testimoial_area">
                                <div class="manager_text">
                                    <div class="single-manager_text">
                                        <img src="<?php echo base_url()."assets/site/" ?>img/news-1.jpg" alt="Images">
                                        <p>Das Pavan-Zentrum in Kleve öffnet am 1. Mai 2017.</p>
                                        <div class="manager">
                                            <h6>Eröffnung!</h6>
                                        </div>
                                    </div>
                                    <div class="single-manager_text">
                                        <img src="<?php echo base_url()."assets/site/" ?>img/news-1.jpg" alt="Images">
                                        <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie              consequat, vel illum dolore eu feugiat nulla molestie consequat, vel illum facilisis at vero.</p>
                                        <div class="manager">
                                            <h4>Rick Grimes</h4>
                                            <h6>News updater</h6>
                                        </div>
                                    </div>
                                    <div class="single-manager_text">
                                        <img src="<?php echo base_url()."assets/site/" ?>img/news-1.jpg" alt="Images">
                                        <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie              consequat, vel illum dolore eu feugiat nulla molestie consequat, vel illum facilisis at vero.</p>
                                        <div class="manager">
                                            <h4>Rick Grimes</h4>
                                            <h6>News updater</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--news end-->   
                        </div>    
                    </div>
                </div>
                </div>
            </div>    
            </div>
        </div>
    </div>
</section>
<!--End join_events area--> 
    
<!--Start how_you_can_help area-->
<section class="environmental_management_area">
    <div class="container">
        <div class="row">
             <div class="col-lg-8 col-md-12"> 
                  <div class="management"><h2>Über Marina <br> und das <span>Pavan-Zentrum </span></h2><div class="border"></div></div>
                  <div class="management_box">
                      <div class="management_recyclin"><i class="fa fa-user"></i></div>
                    <div class="management_text">
                      <h2>Über Marina</h2>
                      <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse  molestie consequat eugiat nulla facilisis at vero. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse  molestie consequat eugiat nulla facilisis at vero.</p>
                    </div>
                  </div>
                  
                  <div class="management_box">
                      <div class="management_recyclin"><i class="fa fa-users"></i></div>
                    <div class="management_text">
                      <h2>Über das Pavan Zentrum</h2>
                      <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse  molestie consequat eugiat nulla facilisis at vero. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse  molestie consequat eugiat nulla facilisis at vero. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse  molestie consequat eugiat nulla facilisis at vero.</p>
                    </div>
                  </div>
              </div>
            <div class="col-lg-4 col-md-12"> 
                <div class="testimonials-wrap ">
            
                    <div class="item">
                        <!-- Startsingle-testimonials -->
                        <div class="single_testimonials">
                            <!-- Start text-box -->
                            <div class="text_box">
                                <p>
                                    <span class="qoute">“</span>
                                    Pemporibus autem quibusdam et aut officiis debitisaut essitatibus consectetuer adipiscing elit.
                                </p>
                            </div>
                            <!-- End text-box -->
                            <!-- Start client-box -->
                            <div class="client-box">
                                <!-- Start img-box -->
                                <div class="img-box">
                                    <img src="<?php echo base_url()."assets/site/" ?>img/testimoial/3.png" alt="">
                                </div>
                                <!--End img-box -->
                                <!--Start info-box -->
                                <div class="info-box">
                                    <h4>Steven Smith</h4>
                                    <span>Manager</span>
                                </div><!--End info-box -->
                            </div>
                            <!--End client-box -->
                        </div>
                        <!-- End single-testimonials -->
                    </div>
        
              
                    <div class="item">
                        <!-- Startsingle-testimonials -->
                        <div class="single_testimonials">
                            <!-- Start text-box -->
                            <div class="text_box">
                                <p>
                                    <span class="qoute">“</span>
                                    Pemporibus autem quibusdam et aut officiis debitisaut essitatibus consectetuer adipiscing elit.
                                </p>
                            </div>
                            <!-- End text-box -->
                            <!-- Start client-box -->
                            <div class="client-box">
                                <!-- Start img-box -->
                                <div class="img-box">
                                    <img src="<?php echo base_url()."assets/site/" ?>img/testimoial/2.png" alt="">
                                </div>
                                <!--End img-box -->
                                <!--Start info-box -->
                                <div class="info-box">
                                    <h4>Peter Parker</h4>
                                    <span>CEO & Founder</span>
                                </div><!--End info-box -->
                            </div>
                            <!--End client-box -->
                        </div>
                        <!-- End single-testimonials -->
                    </div>
                    <div class="item">
                        <!-- Startsingle-testimonials -->
                        <div class="single_testimonials">
                            <!-- Start text-box -->
                            <div class="text_box">
                                <p>
                                    <span class="qoute">“</span>
                                    Pemporibus autem quibusdam et aut officiis debitisaut essitatibus consectetuer adipiscing elit.
                                </p>
                            </div>
                            <!-- End text-box -->
                            <!-- Start client-box -->
                            <div class="client-box">
                                <!-- Start img-box -->
                                <div class="img-box">
                                    <img src="<?php echo base_url()."assets/site/" ?>img/testimoial/3.png" alt="">
                                </div>
                                <!--End img-box -->
                                <!--Start info-box -->
                                <div class="info-box">
                                    <h4>Steven Smith</h4>
                                    <span>Manager</span>
                                </div><!--End info-box -->
                            </div>
                            <!--End client-box -->
                        </div>
                        <!-- End single-testimonials -->
                    </div>
                <div class="item">
                        <!-- Startsingle-testimonials -->
                        <div class="single_testimonials">
                            <!-- Start text-box -->
                            <div class="text_box">
                                <p>
                                    <span class="qoute">“</span>
                                    Pemporibus autem quibusdam et aut officiis debitisaut essitatibus consectetuer adipiscing elit.
                                </p>
                            </div>
                            <!-- End text-box -->
                            <!-- Start client-box -->
                            <div class="client-box">
                                <!-- Start img-box -->
                                <div class="img-box">
                                    <img src="<?php echo base_url()."assets/site/" ?>img/testimoial/2.png" alt="">
                                </div>
                                <!--End img-box -->
                                <!--Start info-box -->
                                <div class="info-box">
                                    <h4>Peter Parker</h4>
                                    <span>CEO & Founder</span>
                                </div><!--End info-box -->
                            </div>
                            <!--End client-box -->
                        </div>
                        <!-- End single-testimonials -->
                    </div>
            
            </div>
            <br>

            
                <div class="environmental_managemen"><img src="<?php echo base_url()."assets/site/" ?>img/topic-2-right.jpg" alt=""></div>
            </div>
        </div>    
    </div>
</section>       
<!--End how_you_can_help area-->
        
<!--Start support_services_area area-->
<section class="support_services_area">
    <div class="container">
        <div class="row">
            <div class="support_title ">
                <h4>“Es ist immerhin dein Geburtsrecht, gesund, glücklich und ganzheitlich zu leben.”<br> <br>
                                           - Y. Bhajan</h4>
            </div>
             
        </div>    
    </div>
</section>       
<!--End support_services_area area-->
    
<!--Start footer area-->
<?php $this->load->view("site/inc/footer"); ?>